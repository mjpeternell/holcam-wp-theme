<?php
/*
 * Search Form - Panorama POV Pages
 */
?>
<form method="get" id="searchform" action= "<?php echo esc_url(home_url('/')); ?>" class= "search-bar" role="search">
    <input type="search" class="field" name="s" value="<?php echo esc_attr(get_search_query()); ?>" id="s" placeholder="<?php esc_attr_e('Search Model # or Key Word &hellip;', 'panorama'); ?>" />
    <button type="submit" class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e('Search', 'panorama'); ?>">
        <i class="fa fa-search" aria-hidden="true"></i>
    </button>
</form>