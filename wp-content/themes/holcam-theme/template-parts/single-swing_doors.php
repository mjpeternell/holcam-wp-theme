<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<?php
// URL Parameters to create return link on archive page
$get_pt = get_post_type();
$get_slug = get_post_field('post_name', get_post());
$params = array('post-type' => $get_pt, 'product-name' => $get_slug);

//ACF Sets Product Options filtering flag

if (get_field('collection_flag')) {
    $collection_flag = get_field('collection_flag');
} else {
    $collection_flag = "";
}
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <section class="column-10 offset-1 single-team-post">
                        <div class="row">
                            <div class="column-6">
                                <?php //the_post_thumbnail(); ?>
                                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                    <li data-thumb="<?php the_post_thumbnail_url('iso-grid-thumb'); ?>" >
                                        <img src="<?php the_post_thumbnail_url('instagram-square-a'); ?>"/>
                                        <?php if (!have_rows('popular_finishes_repeater')): ?>
                                        </li>   
                                    <?php endif; ?>
                                    <?php if (have_rows('popular_finishes_repeater')): ?>
                                        <div class="caption-container">
                                            <div class="caption-content">
                                                <div class="caption-title">Popular Finishes <span>(Click an image to change finish) </span></div>
                                            </div>
                                        </div>
                                        </li>   
                                        <?php
                                        while (have_rows('popular_finishes_repeater')): the_row();
                                            $popular_finish_image = get_sub_field('popular_finish_image');
                                            $popular_finish_label = get_sub_field('popular_finish_label');
                                            $url = $popular_finish_image['url'];
                                            $alt = $popular_finish_image['alt'];
                                            // thumbnail
                                            $thumb_size = 'iso-grid-thumb';
                                            $thumb_img = $popular_finish_image['sizes'][$thumb_size];
                                            // full Img
                                            $full_size = 'instagram-square-a';
                                            $full_img = $popular_finish_image['sizes'][$full_size];
                                            ?>

                                            <li data-thumb="<?php echo $thumb_img; ?>" >
                                                <?php if ($popular_finish_image): ?>
                                                    <img src="<?php echo $full_img; ?>" alt="<?php echo $alt ?>" class="img-responsive"/>
                                                    <div class="caption-container">
                                                        <div class="caption-content">
                                                            <div class="caption-title"><?php echo $popular_finish_label; ?></div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                            <div class="column-6 entry-content">
                                <h1 class="single-title"><?php the_title(); ?></h1>
                                <?php
                                $post_subtitle = get_field("post_subtitle");
                                if ($post_subtitle) {
                                    ?>
                                    <h2 class="single-subtitle"><?php echo $post_subtitle; ?></h2>
                                <?php } else { ?>
                                    <h2 class="single-subtitle">&nbsp;</h2>
                                <?php } ?>
                                <div >
                                    <?php the_content(); ?>
                                </div><!-- .entry-content -->
                            </div>
                        </div>
                    </section>
                    <nav id="prodHotLinks" class="single-product-links column-10 offset-1 single-team-post">
                        <ul class="sp-link-menu">
                            <li class="sp-link-item"><a href="#handles" title="">Handles</a></li>
                            <li class="sp-link-item"><a href="#glassTreatment" title="">Glass Treatment</a></li>
                            <li class="sp-link-item"><a href="#glassTypes" title="">Glass Types</a></li>
                            <li class="sp-link-item"><a href="#metal" title="">Metal</a></li>
                            <li class="sp-link-item"><a href="#documentation" title="">Documentation</a></li>
                        </ul>
                    </nav>
                    <section id="handles" class="column-10 offset-1 single-team-post">
                        <h1 class="section-title">Handles</h1>
                        <?php
                        $team_arg = array(
                            'post_type' => 'handles',
                            //'metal_finish_type' => 'anodized',
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                        );
                        $wp_team_query = new WP_Query($team_arg);
                        //print_r($wp_team_query);
                        $postx_counter = -1;
                        ?>

                        <ul class="product-slides">
                            <?php
                            while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
                                $postx_counter++;
                                ?>
                                <li class="slide" data-count="<?php echo $postx_counter; ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('instagram-square'); ?>
                                        </a>
                                    <?php else : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                                        </a>
                                    <?php endif; ?>
                                    <div class="prod-title"><?php the_title(); ?></div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php wp_reset_postdata(); ?>
                        <?php //if (have_rows('product_details_handles')): ?>   <?php //endif; ?>
                        <!--                            <ul class="product-slides">
                        <?php
                        //while (have_rows('product_details_handles')): the_row();
                        // vars
                        //$product_image = get_sub_field('product_image');
                        //$product_text = get_sub_field('product_text');
                        ?>
                                                            <li class="slide">
                                                                <img src="<?php //echo $product_image['url'];         ?>" alt="<?php //echo $product_image['alt']         ?>" />
                                                                <div class="prod-title"><?php //echo $product_text;         ?></div>
                                                            </li>
                        <?php //endwhile; ?>
                                                    </ul>-->
                        <div class="more-prod-title">View all available handle options. <a href="<?php echo add_query_arg($params, '/handles'); ?>" class="btn-link" title="Handles"> Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>


                    </section>
                    <section id="glassTreatment" class="column-10 offset-1 single-team-post">
                        <h1 class="section-title">Glass Treatments</h1>

                        <?php
                        $team_arg = array(
                            'post_type' => 'glass_treatments',
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                        );
                        $wp_team_query = new WP_Query($team_arg);
                        //print_r($wp_team_query);
                        $postx_counter = -1;
                        ?>

                        <ul class="product-slides">
                            <?php
                            while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
                                $postx_counter++;
                                ?>
                                <li class="slide treatments" data-count="<?php echo $postx_counter; ?>">

                                    <div class="prod-title"></div>
                                    <div class="column-8">
                                        <h3><?php the_title(); ?></h3>
                                        <div class="prod-title"><?php the_content(); ?></div>
                                    </div>
                                    <div class="column-4 vert-align">
                                        <?php if (has_post_thumbnail()) : ?>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <?php the_post_thumbnail('instagram-square'); ?>
                                            </a>
                                        <?php else : ?>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php wp_reset_postdata(); ?>
                        <!--                        <ul class="product-slides">
                        <?php
                        // while (have_rows('product_details_treatments')): the_row();
                        // vars
                        // $product_image = get_sub_field('product_image');
                        //$product_title = get_sub_field('product_title');
                        // $product_text = get_sub_field('product_text');
                        //  
                        ?>
                                                        <li class="slide treatments">
                                                            <div class="column-8">
                                                                <h3><?php //echo $product_title;      ?></h3>
                                                                <div class="prod-title"><?php //echo $product_text;      ?></div>
                                                            </div>
                                                            <div class="column-4 vert-align">
                                                                <img src="<?php //echo $product_image['url'];      ?>" alt="<?php //echo $product_image['alt']      ?>" />
                                                            </div>
                                                        </li>
                        <?php //endwhile;  ?>
                                                </ul>-->
                    </section>
                    <section id="glassTypes" class="column-10 offset-1 single-team-post">
                        <h1 class="section-title">Glass Types</h1>
                        <?php
                        $team_arg = array(
                            'post_type' => 'glass_types',
                            'collections' => $collection_flag,
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                        );
                        $wp_team_query = new WP_Query($team_arg);
                        //print_r($wp_team_query);
                        $postx_counter = -1;
                        ?>

                        <ul class="product-slides">
                            <?php
                            while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
                                $postx_counter++;
                                ?>
                                <li class="slide" data-count="<?php echo $postx_counter; ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('instagram-square'); ?>
                                        </a>
                                    <?php else : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                                        </a>
                                    <?php endif; ?>
                                    <div class="prod-title"><?php the_title(); ?></div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php wp_reset_postdata(); ?>
                        <!--                        <ul class="product-slides">
                        <?php
                        //while (have_rows('product_details_glass')): the_row();
                        // vars
                        //$product_image = get_sub_field('product_image');
                        //$product_text = get_sub_field('product_text');
                        ?>
                                                        <li class="slide">
                                                            <img src="<?php //echo $product_image['url'];        ?>" alt="<?php echo $product_image['alt'] ?>" />
                                                            <div class="prod-title"><?php //echo $product_text;         ?></div>
                                                        </li>
                        <?php //endwhile;     ?>
                                                </ul>-->

                        <!-- pass in the $params array and the URL -->
                        <div class="more-prod-title">View all available glass types. <a href="<?php echo add_query_arg($params, '/glass-types'); ?>" class="btn-link" title="Glass Types"> Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
                    </section>
                    <?php wp_reset_postdata(); ?>
                    <section id="metal" class="column-10 offset-1 single-team-post">
                        <h1 class="section-title">Popular Metal Finishes</h1>
                        <?php
                        $team_arg = array(
                            'post_type' => 'metal_finishes',
                            'metal_finish_type' => 'anodized',
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                        );
                        $wp_team_query = new WP_Query($team_arg);
                        //print_r($wp_team_query);
                        $postx_counter = -1;
                        ?>

                        <ul class="product-slides">
                            <?php
                            while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
                                $postx_counter++;
                                ?>
                                <li class="slide" data-count="<?php echo $postx_counter; ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('instagram-square'); ?>
                                        </a>
                                    <?php else : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                                        </a>
                                    <?php endif; ?>
                                    <div class="prod-title"><?php the_title(); ?></div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <div class="more-prod-title">View all available metal finishes. <a href="<?php echo add_query_arg($params, '/metal-finishes'); ?>" class="btn-link" title="Metal Finishes"> Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
                        <?php wp_reset_postdata(); ?>
                        <!--                        <ul class="product-slides">
                        <?php
                        //while (have_rows('product_details_metal')): the_row();
                        // vars
                        //$product_image = get_sub_field('product_image');
                        //$product_text = get_sub_field('product_text');
                        ?>
                                                        <li class="slide">
                                                            <img src="<?php //echo $product_image['url'];        ?>" alt="<?php //echo $product_image['alt']        ?>" />
                                                            <div class="prod-title"><?php //echo $product_text;         ?></div>
                                                        </li>
                        <?php //endwhile;     ?>
                                                </ul>-->
                    </section>
                    <section id="documentation" class="column-10 offset-1 single-team-post">
                        <h1 class="section-title">Documentation</h1>
                        <ul class="product-slides">
                            <?php
                            while (have_rows('product_details_docs')): the_row();
                                // vars
                                $document_name = get_sub_field('document_name');
                                $document_link = get_sub_field('document_link');
                                ?>
                                <li class="slide docs">
                                    <a href="<?php echo $document_link; ?>" ><?php echo $document_name; ?></a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </section>
                    <footer class="column-10 offset-1 align-center">
                        <?php edit_post_link(__('Edit', 'panorama'), '<span class="edit-link">', '</span>'); ?>
                    </footer>
                </article><!-- #post-<?php the_ID(); ?> -->
            <?php endwhile; // end of the loop.          ?>


        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php get_footer(); ?>