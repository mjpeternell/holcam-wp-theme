<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" class="row search-list-item">
    <div class="column-2">
        <div class="blog-image">
            <?php if (has_post_thumbnail()) : ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail('instagram-square'); ?>
                </a>
            <?php else : ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="column-10 entry-summary">
        <h1 class="entry-title"><a href="<?php the_permalink(); ?>" class="name truncate-120" title="<?php echo esc_attr(sprintf(__('%s', 'panorama'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
        <?php
        $post_subtitle = get_field("post_subtitle");
        if ($post_subtitle) {
            ?>
            <h2 class="single-subtitle"><?php echo $post_subtitle; ?></h2>
        <?php } ?>
        <?php
        $current_part_number = get_field("current_part_number");
        $obsolete_part_number = get_field("obsolete_part_number");
        if ($current_part_number && $obsolete_part_number) {
            ?>
            <div class="part-number">Part Number: <?php echo $current_part_number; ?> <?php echo ' / ' . $obsolete_part_number; ?></div>
        <?php } ?>
        <?php the_excerpt(); ?>
        <footer class="entry-meta">
            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>', 'panorama'), '<span class="edit-link">', '</span>'); ?>
        </footer><!-- .entry-meta -->
    </div>
</article><!-- #post-<?php the_ID(); ?> -->


