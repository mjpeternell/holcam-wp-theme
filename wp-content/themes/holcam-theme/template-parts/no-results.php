<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>

<div class="column-12 text-center">
    <header class="page-header">
        <h1 class="page-title"><?php _e('Nothing Found', 'panorama'); ?></h1>
    </header><!-- .entry-header -->
</div>
<div class="column-10 offset-1 board-list">
    <?php if (is_home() && current_user_can('publish_posts')) : ?>

        <p><?php printf(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'panorama'), admin_url('post-new.php')); ?></p>

    <?php elseif (is_search()) : ?>

        <p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'panorama'); ?></p>
        <?php get_search_form(); ?>

    <?php else : ?>

        <p><?php _e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'panorama'); ?></p>
        <?php get_search_form(); ?>

    <?php endif; ?>
</div>
