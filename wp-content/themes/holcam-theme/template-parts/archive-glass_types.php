<?php
/**
 * The template for displaying CPT metal_finishes.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<?php
$product_name = get_query_var('product-name');
$post_type = get_query_var('post-type');
$post_type = str_replace('_', '-', $post_type);
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <?php if (have_posts()) : ?>
                <div class="column-12 text-center">
                    <header class="page-header">
                        <h1 class="page-title">
                            <?php
                            $postType = get_queried_object();
                            echo esc_html($postType->labels->name);
                            ?>
                        </h1>
                    </header><!--.page-header -->
                </div>
                <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
                <nav class="column-10 offset-1 breadcrumb-nav"><a href="<?php echo '/' . $post_type . '/' . $product_name; ?>">Return to <?php echo $product_name; ?></a></nav>
                <div class = "column-10 offset-1 search-list top">
                    <div class="row">
                        <?php
                        $postx_counter = 0;
                        while (have_posts()) : the_post();
                            get_template_part('template-parts/holcam-metal-finishes');
                            if ($postx_counter % 4 == 3) {
                                echo '</div><div class="row">';
                            }
                            $postx_counter++;
                        endwhile;
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
        <?php get_template_part('inc/global', 'contact-cta'); ?>
    </main>
</div>

<?php get_footer(); ?>