<?php

/*
 * Flexible layout for ACF
 */
?>

<?php

// check if the flexible content field has rows of data
if (have_rows('flex_layouts')):
    // loop through the rows of data
    $flex_layout = "";
    while (have_rows('flex_layouts')) : the_row();
        $flex_layout++;
        ?>
        <?php

        if (get_row_layout() == '12_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-12 entry-content\">";
            the_sub_field('12_column_editor');
            echo "</div>";
            echo "</div>";
        elseif (get_row_layout() == '444_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-4 entry-content\">";
            the_sub_field('4_column_editor_left');
            echo "</div>";
            echo "<div class=\"column-4 entry-content\">";
            the_sub_field('4_column_editor_mid');
            echo "</div>";
            echo "<div class=\"column-4 entry-content\">";
            the_sub_field('4_column_editor_right');
            echo "</div>";
            echo "</div>";
        elseif (get_row_layout() == '66_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-6 entry-content\">";
            the_sub_field('6_column_editor_left');
            echo "</div>";
            echo "<div class=\"column-6 entry-content\">";
            the_sub_field('6_column_editor_right');
            echo "</div>";
            echo "</div>";
        elseif (get_row_layout() == '84_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-8 entry-content\">";
            the_sub_field('8_column_editor');
            echo "</div>";
            echo "<div class=\"column-4 entry-content\">";
            the_sub_field('4_column_editor');
            echo "</div>";
            echo "</div>";
        elseif (get_row_layout() == '75_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-7 entry-content\">";
            the_sub_field('7_column_editor');
            echo "</div>";
            echo "<div class=\"column-5 entry-content\">";
            the_sub_field('5_column_editor');
            echo "</div>";
            echo "</div>";
        elseif (get_row_layout() == '57_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-5 entry-content\">";
            the_sub_field('5_column_editor');
            echo "</div>";
            echo "<div class=\"column-7 entry-content\">";
            the_sub_field('7_column_editor');
            echo "</div>";
            echo "</div>";
        elseif (get_row_layout() == '48_columns_layout'):
            echo '<div id="FlexLayout1-' . $flex_layout . '" class="wysiwyg-row">';
            echo "<div class=\"column-4 entry-content\">";
            the_sub_field('4_column_editor');
            echo "</div>";
            echo "<div class=\"column-8 entry-content\">";
            the_sub_field('8_column_editor');
            echo "</div>";
            echo "</div>";
        endif;
        ?>
        <?php

    endwhile;
else :
    echo "<h4>Add Some content</h4>";
endif;
?>