<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


        <!--                            <div class="blog-image">
        <?php
//        if (has_post_thumbnail($wp_blog_query->ID)) {
//            echo '<a href="' . get_permalink($wp_blog_query->ID) . '" title="' . esc_attr($wp_blog_query->post_title) . '">';
//            echo get_the_post_thumbnail($wp_blog_query->ID, 'hero-cta-680');
//            echo '</a>';
//        } else {
//            echo '<a href="' . get_permalink($wp_blog_query->ID) . '" title="' . esc_attr($wp_blog_query->post_title) . '">';
//            echo '<img src="/wp-content/themes/panorama-theme/assets/images/holcam-blog-placeholder-image.jpg" class="img-responsive"  alt="PlaceHolder Image"/>';
//            echo '</a>';
//        }
        ?>
                                    </div>-->
        <header class="entry-header">
          
            <h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            <?php
            //$author = get_the_author();
            //echo '<span class="author">By: ' . $author . '</span>';
            ?> 
            <?php
            $post_pen_name = get_field('post_pen_name');
            if ($post_pen_name) :
                ?>
                <span class="author">By: <?php echo $post_pen_name; ?></span>
            <?php else: ?>
                <span class="author">&nbsp;</span>
            <?php endif; ?>
        </header>
        <div class="blog-content">
            <?php the_excerpt(); ?>
        </div>

        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<footer class="entry-meta">', '</footer>', 0, 'post-edit-link btn btn-default'); ?>
