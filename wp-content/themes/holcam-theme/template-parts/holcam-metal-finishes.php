<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" class="column-3 search-list-item products" >

    <div class=" blog-image">
        <?php if (has_post_thumbnail()) : ?>
            <?php the_post_thumbnail('instagram-square'); ?>
        <?php else : ?>
            <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
        <?php endif; ?>
    </div>
    <div class=" entry-summary">
        <h1 class="entry-title"><?php the_title(); ?></h1>

        <?php
        $terms = get_the_terms(get_the_ID(), 'collections');
        if ($terms && !is_wp_error($terms)) :
            $draught_links = array();
            foreach ($terms as $term) {
                $draught_links[] = $term->name;
            }
            $on_draught = join(", ", $draught_links);
            ?>
            <p class="beers draught"><b>Collection Availability:</b> <span><?php echo $on_draught; ?></span></p>
        <?php endif; ?>
        <?php
        $mf_terms = get_the_terms(get_the_ID(), 'metal_finish_type');
        if ($mf_terms && !is_wp_error($mf_terms)) :
            $mf_links = array();
            foreach ($mf_terms as $mf_term) {
                $mf_links[] = $mf_term->name;
            }
            $mf_draught = join(", ", $mf_links);
            ?>
            <p class="beers draught"><b>Metal Finish Type:</b> <span><?php echo $mf_draught; ?></span></p>
        <?php endif; ?>
        <?php //the_excerpt(); ?>
        <footer class="entry-meta">
            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>', 'panorama'), '<span class="edit-link">', '</span>'); ?>
        </footer><!-- .entry-meta -->
    </div>
</article><!-- #post-<?php the_ID(); ?> -->


