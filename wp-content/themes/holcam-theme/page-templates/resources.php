<?php
/**
 * Template Name: Resources Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <section class="col-fullbleed holcam-blog">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        get_template_part('template-parts/content', 'default-page');
                    endwhile;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="col-fullbleed install-docs">
        <div class="col-full">
            <div class="column-10 offset-1">

                <ul class="download-links">
                    <?php
                    $rlink_url = get_field('resources_link_url');
//                    echo '<pre>';
//                    print_r($rlink_url);
//                    echo '</pre>';
                    if ($rlink_url):
                        ?>
                        <header class="single-header">
                            <h2 class="single-subtitle">Visit our Resource Pages</h2>
                        </header>
                        <ul>
                            <?php foreach ($rlink_url as $rurl): ?>
                                <?php 
                                $rurl = rtrim($rurl,"/");
                                $s1clean = str_replace('-', ' ', $rurl);
                                $str = preg_replace('/^.*\/\s*/', '', $s1clean);
                                ?>
                                <li>
                                    <a href="<?php echo $rurl ?>"><?php echo ucwords($str); ?> <i class="fa fa-link" aria-hidden="true"></i></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php
get_footer();


