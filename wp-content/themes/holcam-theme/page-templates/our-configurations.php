<?php
/**
 * Template Name: Our Configs Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <section class="col-fullbleed holcam-blog">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        get_template_part('template-parts/content', 'default-page');
                    endwhile;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="col-fullbleed install-docs">
        <div class="col-full">
            <div class="column-10 offset-1">
                <header class="single-header">
                    <h2 class="single-subtitle">The Complete List of Configurations</h2>
                </header>
                <ul class="download-links config">
                    <?php
                    while (have_rows('configuration_types')): the_row();
                        $description = get_sub_field('description');
                        $title = get_sub_field('configuration_title');
                        $icon = get_sub_field('icon');
                        $url = $icon['url'];
//                        echo '<pre>';
//                        print_r($link_url);
//                         echo '</pre>';
                        if ($icon) {

                        }
                        ?>
                        <li class="link-item ">
                            <div class="column-2">
                                <img src="<?php echo $url;?>" />
                            </div> 
                            <div class="column-8">
                                <div class="title"><?php echo $title; ?></div>
                                <div class="descrition"><?php echo $description; ?></div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php
get_footer();


