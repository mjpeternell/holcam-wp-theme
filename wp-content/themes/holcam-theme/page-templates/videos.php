<?php
/**
 * Template Name: Video Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <section class="col-fullbleed holcam-blog">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        get_template_part('template-parts/content', 'default-page-no-sidebar');
                    endwhile;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="col-fullbleed install-docs">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php get_template_part('inc/acf-oembed-repeater'); ?>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php
get_footer();


