<?php

/**
 * Template Name: Home Page
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since  Panorama 1.0
 */
get_header();
get_template_part('inc/hero');
?>
<?php
while (have_posts()) : the_post();
    get_template_part('inc/home', 'intro-content');
endwhile;
?>
<?php get_template_part('inc/holcam', 'door-type-cta'); ?>
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/holcam', 'collections'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php

get_footer();


