<?php
/**
 * Template Name: Dealer Tools Child Page Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <?php
    global $current_user;
     wp_get_current_user();
    if (user_can($current_user, "dealer") || user_can($current_user, "administrator")) {
        ?>
        <section class="col-fullbleed holcam-blog">
            <div class="col-full">
                <div class="column-10 offset-1">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            get_template_part('template-parts/content', 'default-page');
                        endwhile;
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="col-fullbleed install-docs">
            <div class="col-full">
                <div class="column-10 offset-1">
                    <header class="single-header">
                        <h2 class="single-subtitle">Select a Document to Download</h2>
                    </header>
                    <ul class="download-links">
                        <?php
                        while (have_rows('download_links')): the_row();
                            $link_url = get_sub_field('link_url');
                            if ($link_url) {
                                $my_id2 = $link_url['ID'];
                                $url_file = $link_url['url'];
                                $title_file = $link_url['title'];
                                $caption_file = $link_url['caption'];
                                $desc = $link_url['description'];
                                $icon = $link_url['icon'];
                            }
                            ?>
                            <li class="link-item ">
                                <div class="column-1">
                                    <?php if (wp_get_attachment_image($link_url['ID'])) { ?>
                                        <?php echo wp_get_attachment_image($link_url['ID'], 'thumbnail', "", array("class" => "img-thumbnail")); ?>
                                    <?php } else { ?>
                                        <img src="<?php echo $icon; ?>" class="img-responsive"/>
                                    <?php } ?>
                                </div> 
                                <div class="column-10">
                                    <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>"><?php echo $title_file; ?> <i class="fa fa-download" aria-hidden="true"></i></a>
                                    <div class="descrition"><?php echo $desc; ?></div>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </section>

    <?php } else { ?>
        <section class="col-fullbleed holcam-blog">
            <div class="col-full">
                <div class="column-10 offset-1">
                    <header class="entry-header page-header">
                        <h1 class="entry-title">Dealer Tools Login</h1>
                        <p class="subtitle"><strong>You Need To Be Logged into the site inorder to gain access.</strong></p>
                    </header>
                    <div class="">
                        <p>Returning users please login below. First time users, skip to Dealer Request Form.</p>
                        <p><a class="btn btn-primary btn-sm" href="<?php echo wp_login_url(get_permalink()); ?>" role="button">Login <i class="fa fa-angle-double-right"></i></a>
                            <br/><br/><br/>
                        <h3>Dealer Request Form</h3>
                        <p>Please fill-out this quick request form for:</p>
                        <ul>
                            <li>Account setup to gain access to Dealers Tools Login</li>
                            <li>Information on becoming an Agalite dealer</li>
                            <li>Additional information that you have not found on this site</li>
                            <li>Recommending changes to this site or Agalite products</li>
                            <li>Giving general feedback</li>
                        </ul>
                        <br/>
                        <p><a href="/dealer-request-form/" class="btn btn-primary btn-sm">Dealer Request Form <i class="fa fa-angle-double-right"></i></a></p>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
</div>
<?php
get_footer();


