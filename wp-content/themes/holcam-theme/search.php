<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">

            <?php if (have_posts()) : ?>
                <div class="column-12 text-center">
                    <header class="page-header">
                        <h1 class="page-title"><?php printf(__('Search Results for: %s', 'panorama'), '<span>' . get_search_query() . '</span>'); ?></h1>
                    </header><!-- .page-header -->

                </div>   
                <div class="column-10 offset-1 search-list top">
                    <?php //panorama_content_nav('nav-above'); ?>
                    <?php /* Start the Loop */ ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('template-parts/search-results', 'search'); ?>      
                        <?php //get_template_part('content', 'search'); ?>

                    <?php endwhile; ?>

                    <?php panorama_content_nav('nav-below'); ?>
                </div>
                <div class="column-10 offset-1 search-list bot">
                    <div class="blog-column-6">
                        <h3>Couldn't Find Something? Try Again.</h3>
                    </div>
                    <div class="blog-column-6">
                        <?php get_template_part('template-parts/panorama', 'search-form'); ?>
                    </div>
                </div>
                <?php else : ?>
                    <?php get_template_part('template-parts/no-results', 'search'); ?>      
                <?php endif; ?>

            </div>
            <?php get_template_part('inc/global','where-to-buy-cta'); ?>
            <?php get_template_part('inc/global','contact-cta'); ?>
    </main>
</div>

<?php get_footer(); ?>