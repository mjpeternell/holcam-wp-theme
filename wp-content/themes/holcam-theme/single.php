<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">

        <?php if (has_post_thumbnail()) { ?>
            <div id="hero" class="hero-alt jumbotron parallax-window"  data-natural-width="1280" data-natural-height="1080" data-image-src="<?php the_post_thumbnail_url(); ?>" data-speed="0.2" data-bleed="0" data-parallax="scroll"></div>
        <?php } ?>
        <div class="col-fullbleed white">
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-full single-blog-nav top">
                    <div class="column-10 offset-1 text-left">
                        <?php panorama_content_nav('nav-above'); ?>
                    </div>
                </div>
                <div class="col-full single-blog-post">
                    <div class="column-10 offset-1">
                        <div class="column-8">
                            <?php get_template_part('template-parts/content', 'single'); ?>
                        </div>
                        <div class="column-4 sidebar">
                            <?php get_sidebar(); ?>
                        </div>
                    </div>
                </div>

                <div class="col-full single-blog-nav below">
                    <div class="column-12 text-center">
                        <?php panorama_content_nav('nav-below'); ?>
                    </div>
                </div>
            <?php endwhile; // end of the loop.  ?>
        </div>

</div><!-- #primary .content-area -->
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php get_footer(); ?>