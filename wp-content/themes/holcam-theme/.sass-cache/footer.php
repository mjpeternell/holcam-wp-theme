<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>

</div><!-- #main .site-main -->
</div><!-- #page .hfeed .site -->
<footer id="colophon" class="site-footer container-fluid" role="contentinfo">
    <div class="site-info">
        <?php
        $footer_args = array(
            'theme_location' => 'footer',
            'menu_class' => 'footer-menu',
            'depth' => 1,
            'walker' => new rooster_park_navwalker()
        );
        wp_nav_menu($footer_args);
        ?>
        <?php //do_action('panorama_credits', 'option'); ?>
        <div class="footer-secondary-links">
            <?php if (have_rows('social_links', 'option')): ?>
                <ul class="social-links-list">
                    <?php
                    while (have_rows('social_links', 'option')): the_row();
                        // vars
                        $social_icon = get_sub_field('social_icon', 'option');
                        $social_title = get_sub_field('social_title', 'option');
                        $social_icon_url = get_sub_field('social_icon_url', 'option');
                        ?>
                        <li class="social-link-item">  <a href="<?php echo $social_icon_url; ?>" title="<?php echo $social_title; ?>" target="_blank">
                                <i class="fa <?php echo $social_icon; ?>" aria-hidden="true"></i>    
                            </a>
                            <?php if ($social_icon_url): ?>

                            <?php endif; ?>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>

    </div>
    <div class="copyright">
        <div class="copyright-inner">
            <?php esc_attr_e('Copyright ©', 'preference'); ?><?php _e(date('Y')); ?> <?php bloginfo('name'); ?>  All Rights Reserved
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script>

    // Set options
    var options = {
        offset: 300,
        offsetSide: 'top',
        classes: {
            clone: 'banner--clone',
            stick: 'banner--stick',
            unstick: 'banner--unstick'
        }
    };
        // Initialise with options
    var banner = new Headhesive('.banner', options);
    //console.log(banner);
    // Headhesive destroy
    // banner.destroy();
    
</script>
<div class="scroll-to-top">
    <i class="fa fa-angle-up"></i>
</div>
</body>
</html>