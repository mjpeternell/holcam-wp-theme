<?php
/**
 * Template Name: POV Template - Blog
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since  Panorama 1.0
 */
get_header();
?>
<?php get_template_part('inc/hero-parallax'); ?>
<?php
// Start the loop.
if (have_posts()) :
    ?>
<!--    <section id="PanoramaBlog" class="col-fullbleed holcam-blog">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php
                while (have_posts()) : the_post();
                    // Include the page content template.
                    //get_template_part('content', 'page');
                    // End the loop.
                    the_content();
                endwhile;
                ?>
            </div>
        </div>
    </section>-->
    <?php endif; ?>


    <section id="PanoramaBlog" class="col-fullbleed holcam-blog">
        <div class="col-full">
            <div class="column-10 offset-1">

                <?php
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                $blog_arg = array(
                    'post_type' => 'point_of_view',
                    'orderby' => 'post_date',
                    'order' => 'date',
                    'post_status' => 'publish',
                    'posts_per_page' => 10,
                    'paged' => $paged,
                );
                $wp_blog_query = new WP_Query($blog_arg);
                $postx_counter = -1;
                if (have_posts()) :
                    while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                        $image_id = get_post_thumbnail_id();
                        $imagesize = "thumbnail";

                        $postx_counter++;
                        $offset = "";
                        if ($postx_counter == 0) {
                            $offset = "col-lg-offset-2";
                        } else {
                            $offset = "col-lg-offset-1";
                        }
                        ?>
                        <div class="blog-column-6 blog-tile" data-count="<?php echo $postx_counter; ?>">
                            <article class="tile-inner" >
                                <div class="blog-image">
                                    <?php
                                    if (has_post_thumbnail($wp_blog_query->ID)) {
                                        echo '<a href="' . get_permalink($wp_blog_query->ID) . '" title="' . esc_attr($wp_blog_query->post_title) . '">';
                                        echo get_the_post_thumbnail($wp_blog_query->ID, 'hero-cta-680');
                                        echo '</a>';
                                    } else {
                                        echo '<a href="' . get_permalink($wp_blog_query->ID) . '" title="' . esc_attr($wp_blog_query->post_title) . '">';
                                        echo '<img src="/wp-content/themes/panorama-theme/assets/images/holcam-blog-placeholder-image.jpg" class="img-responsive"  alt="PlaceHolder Image"/>';
                                        echo '</a>';
                                    }
                                    ?>
                                </div>
                                <header class="entry-header"> <h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></header>
                                <div class="blog-content">
                                    <?php the_excerpt(); ?>
                                </div>
                                
                                    <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<footer class="entry-meta">', '</footer>', 0, 'post-edit-link btn btn-default'); ?>
                                
                            </article>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php if ($wp_blog_query->max_num_pages > 1) { // check if the max number of pages is greater than 1      ?>
                    <nav class="post-navigation">
                        <div class="nav-previous">
                            <?php echo get_next_posts_link('<i class="fa fa-angle-left" aria-hidden="true"></i> Older Entries', $wp_blog_query->max_num_pages); // display older posts link       ?>
                        </div>
                        <div class="nav-next">
                            <?php echo get_previous_posts_link('Newer Entries <i class="fa fa-angle-right" aria-hidden="true"></i>'); // display newer posts link       ?>
                        </div>
                    </nav>
                <?php } ?>
            </div>
        </div>

        <?php wp_reset_postdata(); ?>


    </section>
    <?php get_template_part('inc/panorama', 'contact-cta'); ?>
    <?php
    get_footer();
    