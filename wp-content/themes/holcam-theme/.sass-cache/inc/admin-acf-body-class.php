<?php

/**
 * Set CSS Color class in <body> tag using ACF
 */
function public_acf_body_class_filter($classes) {
    $page_color_theme = get_field('page_color_theme', get_queried_object_id());
    if ($page_color_theme) {
        $page_color_theme = esc_attr(trim($page_color_theme));
        $classes[] = $page_color_theme;
    }
    return $classes;
}

add_filter('body_class', 'public_acf_body_class_filter');