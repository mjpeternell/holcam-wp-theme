<section id="Cool-Stuff" class="col-fullbleed aluminium partners">
    <div class="col-full">
        <h2 class="section-header white align-center">Our Partners</h2>
        <div class="column-12 text-center"> 
            <?php if (have_rows('our_partners_repeater', 'option')): ?>
                <ul class="icon-list">
                    <?php
                    while (have_rows('our_partners_repeater', 'option')): the_row();
                        // vars
                        $partners_image = get_sub_field('partners_image', 'option');
                        $partners_title = get_sub_field('partners_title', 'option');                       
                        $partners_url = get_sub_field('partners_url', 'option');
                        ?>

                        <li>
                            <?php if ($partners_image): ?>
                                <?php if ($partners_url): ?>
                                <a href="<?php echo $partners_url; ?>" title="<?php echo $partners_title; ?>" target="_blank">
                                <?php endif; ?>
                                    <img src="<?php echo $partners_image; ?>" alt="<?php echo $partners_title; ?>" class="img-responsive" />
                                <?php if ($partners_url): ?>
                                </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</section>