<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <?php if (has_post_thumbnail()) { ?>
            <div id="hero" class="hero-alt jumbotron parallax-window"  data-natural-width="1280" data-natural-height="1080" data-image-src="<?php the_post_thumbnail_url(); ?>" data-speed="0.2" data-bleed="0" data-parallax="scroll"></div>
        <?php } ?>
        <div class="col-fullbleed white">
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-full single-blog-nav top">
                    <div class="column-10 text-left">
                        <a href="/blog" type="button" class="btn btn-link"><i class="fa fa-angle-left" aria-hidden="true"></i> Back To Blog</a>
                        <?php //rooster_park_content_nav('nav-above');  ?>
                    </div>
                </div>
                <div class="col-full single-blog-post">
                    <div class="column-10">
                        <?php get_template_part('content', 'single'); ?>
                    </div>
                </div>
                <div class="col-full single-blog-nav below">
                    <div class="column-12 text-center">
                        <?php rooster_park_content_nav('nav-below'); ?>
                    </div>
                </div>
            <?php endwhile; // end of the loop.  ?>
        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php
//get_template_part('inc/footer-cta-single');
get_footer();
?>