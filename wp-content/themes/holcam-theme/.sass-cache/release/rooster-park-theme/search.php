<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="col-full rooster-search">
                <?php if (have_posts()) : ?>

                <header class="page-header">
                    <h1 class="page-title"><?php printf(__('Search Results for: %s', 'panorama'), '<span>' . get_search_query() . '</span>'); ?></h1>
                </header><!-- .page-header -->

                <?php rooster_park_content_nav('nav-above'); ?>

                <?php /* Start the Loop */ ?>
                <?php while (have_posts()) : the_post(); ?>

                    <?php get_template_part('content', 'search'); ?>

                <?php endwhile; ?>

                <?php rooster_park_content_nav('nav-below'); ?>

                <?php else : ?>

                <?php get_template_part('no-results', 'search'); ?>

                <?php endif; ?>
                <?php //get_sidebar(); ?>     
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>