/*! panorama Park Theme  - v0.1.0 - 2016-11-22
 * http://roosterparkDotcom
 * Copyright (c) 2016; * Licensed GPLv2+ */



/*! RoosterPark google-maps.js - v0.1.0 - 2016-04-01
 * http://roosterparkDotcom
 * Copyright (c) 2016; * Licensed GPLv2+ */
(function ($) {

    /*
     *  new_map
     *
     *  This function will render a Google Map onto the selected jQuery element
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$el (jQuery element)
     *  @return	n/a
     */

    function new_map($el) {

        // var
        var $markers = $el.find('.marker');

        // vars
        var args = {
            zoom: 16,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        // create map
        var map = new google.maps.Map($el[0], args);

        // add a markers reference
        map.markers = [];

        // add markers
        $markers.each(function () {
            add_marker($(this), map);
        });

        // center map
        center_map(map);

        // return
        return map;

    }

    /*
     *  add_marker
     *
     *  This function will add a marker to the selected Google Map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$marker (jQuery element)
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    function add_marker($marker, map) {

        // var
        var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

        // create marker
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'panorama Park HQ'
        });

        // add to array
        map.markers.push(marker);
        var contentString = '<div id="content" class="gmaps-info-win">' +
                '<div id="siteNotice" class="gmaps-info-content">' +
                '<h1 id="firstHeading" class="gmaps-info-title">panorama Park HQ</h1>' +
                '<p><b>Address:</b> 2100 Westlake Ave N #107<br>Seattle, WA 98109' +
                '<br><b>Phone:</b> 206-801-0189' +
                '<br><b>Email:</b> <a href="mailto:us@roosterpark.com">us@roosterpark.com</a>' +
                '</p></div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

    }

    /*
     *  center_map
     *
     *  This function will center the map, showing all markers attached to this map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    function center_map(map) {

        // vars
        var bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        $.each(map.markers, function (i, marker) {
            var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
            bounds.extend(latlng);

        });

        // only 1 marker?
        if (map.markers.length === 1)
        {
            // set center of map
            map.setCenter(bounds.getCenter());
            map.setZoom(16);
        } else
        {
            // fit to bounds
            map.fitBounds(bounds);
        }
    }

    /*
     *  document ready
     *
     *  This function will render each map when the document is ready (page has loaded)
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	5.0.0
     *
     *  @param	n/a
     *  @return	n/a
     */
    // global var
    var map = null;

    $(document).ready(function () {
        $('.acf-map').each(function () {
            // create map
            map = new_map($(this));
        });
    });
})(jQuery);