<?php
/*
 * Case Study Loop: includes ACF, title, copy and link.
 * Pages: Homepage,
 */
?>

<section id="Case-Studies-Home" class="col-fullbleed white three-col-section">
    <div class="col-full">
    <h1 class="section-header orange">Results you can measure.</h1>
    <?php
    $cs_arg = array(
        'post_type' => 'case_studies',
        'posts_per_page' => 3,
        'orderby' => 'post_date',
        'order' => 'date',
        'post_status' => 'publish',
    );
    $wp_cs_query = new WP_Query($cs_arg);
    $post_cs_counter = -1;
    ?>
    <div class="flexy max">
    <?php
    while ($wp_cs_query->have_posts()) : $wp_cs_query->the_post();
        $post_cs_counter++;
        $offset = "";
        if ($post_cs_counter == 0) {
            $offset = "col-lg-offset-2";
        } else {
            $offset = "col-lg-offset-1";
        }
        ?>
        <div class="tile cs" data-count="<?php echo $post_cs_counter; ?>">
        <article class="tile-inner entry-article" >
            <header class="entry-header">
                <?php the_title('<h1 class="title">', '</h1>'); ?>
                <?php
                if (get_field('home_page_cs_sub_title')) {
                    echo '<h2 class="h2-truncate">' . get_field('home_page_cs_sub_title') . '</h2>';
                }
                ?>
            </header>
            <div class="content">
                <?php 
                $acf_excerpt = get_field("home_page_cs_excerpt");
                if($acf_excerpt) {
                    echo $acf_excerpt;  
                }
                ?>
            
            </div>
            <footer class="entry-footer">
                <a href="<?php the_permalink(); ?>" type="button" class="btn btn-lg btn-primary">Read The Case Study</a>
                <?php edit_post_link( __('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default' );?>
            </footer>
            
        </article> 
        </div>
    <?php endwhile; ?>
        </div>
    <?php wp_reset_postdata(); ?>
    </div>
</section>

