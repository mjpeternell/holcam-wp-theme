<?php
/*
 * We Do Both: includes Recruit and Development Posts, title, content and link.
 * Pages: Homepage,
 * 
 */
$my_post_thumb_img = "instagram-square";
?>
<section id="recruiting" class="white text-center two-col-section recruit col-fullbleed">
    <div class="col-full">
        <h1 class="section-header orange">Recruiting</h1>
        <!--<h2 class="section-subheader"></h2>-->
        <article class="column-2">
            <?php
            $my_dev_query = new WP_Query('name=fulltime');
            while ($my_dev_query->have_posts()) {
                $my_dev_query->the_post();
                ?>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title() ?></h1>
                </header>
                <div class="content"><?php the_content(); ?></div>
                <footer><a class="btn btn-lg btn-primary" type="button" href="/capabilities">Learn more</a></footer>
                <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>

            <?php } ?>
            <?php wp_reset_postdata(); ?>
        </article>
        <article class="column-2">
            <?php
            $my_rec_query = new WP_Query('name=capabilities-recruitment');
            while ($my_rec_query->have_posts()) {
                $my_rec_query->the_post();
                ?>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title() ?></h1>
                </header>
                <div class="content"><?php the_content(); ?></div>
                <footer><a class="btn btn-lg btn-primary" type="button" href="/recruitment">Our process</a></footer>
                <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
            <?php } ?>
            <?php wp_reset_postdata(); ?>
        </article>
    </div>
</section>


