<section id="Cool-Stuff" class="col-fullbleed ocean companies">
    <div class="col-full">
        <h1 class="section-header orange">We’ve done cool stuff for these brands.</h1>
        <article class="column-12 text-center"> 
            <ul class="icon-list">
                <li><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/ms-rooster-companies.png"; ?>" class="img-responsive" alt="Microsoft"></li>
                <li><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/disney-rooster-companies.png"; ?>" class="img-responsive" alt="Disney"></li>
                <li><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/espn-rooster-companies.png"; ?>" class="img-responsive" alt="ESPN"></li>
                <li><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/digg-rooster-companies.png"; ?>" class="img-responsive" alt="Digg"></li>
                <li><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/groupon-rooster-companies.png"; ?>" class="img-responsive" alt="Groupon"></li>
                <li><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/moz-rooster-companies.png"; ?>" class="img-responsive" alt="Moz"></li>
            </ul>
        </article>
    </div>
</section>