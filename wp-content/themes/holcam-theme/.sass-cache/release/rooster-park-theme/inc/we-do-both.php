<?php
/*
 * We Do Both: includes Recruit and Development Posts, title, content and link.
 * Pages: Homepage,
 * 
 */
$my_post_thumb_img = "instagram-square";
?>
<section id="We-Do-Both" class="midnight text-center two-col-section we-do col-fullbleed">
    <div class="col-full">
        <h1 class="section-header orange">We Do Both.</h1>
        <h2 class="section-subheader">Whether you need people, or software for your company, we carefully select and vet our candidates, full-time staff, and contract staff.</h2>
        <div class="inner-col-full"><article class="column-2">
                <?php
                $my_dev_query = new WP_Query('name=development');
                while ($my_dev_query->have_posts()) {
                    $my_dev_query->the_post();
                    ?>
                    <header class="entry-header">
                        <?php if (has_post_thumbnail()) { ?>
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($my_post_thumb_img, array('class' => "svg-thumbnail ")); ?></a>
                        <?php } else { ?>
                            <a href="<?php the_permalink(); ?>"><img src="https://placeholdit.imgix.net/~text?txtsize=12&txt=120%C3%97120&w=120&h=120" class="img-thumbnail"/></a>
                        <?php } ?>
                        <h1 class="entry-title"><?php the_title() ?></h1>
                    </header>
                    <div class="content"><?php the_content(); ?></div>
                    <footer><a class="btn btn-lg btn-primary" type="button" href="/capabilities">Learn more</a></footer>
                    <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>

                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </article>
            <article class="column-2">
                <?php
                $my_rec_query = new WP_Query('name=recruitment');
                while ($my_rec_query->have_posts()) {
                    $my_rec_query->the_post();
                    ?>
                    <header class="entry-header">
                        <?php if (has_post_thumbnail()) { ?>
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($my_post_thumb_img, array('class' => "svg-thumbnail ")); ?></a>
                        <?php } else { ?>
                            <a href="<?php the_permalink(); ?>"><img src="https://placeholdit.imgix.net/~text?txtsize=12&txt=120%C3%97120&w=120&h=120" class="img-thumbnail"/></a>
                        <?php } ?>
                        <h1 class="entry-title"><?php the_title() ?></h1>
                    </header>
                    <div class="content"><?php the_content(); ?></div>
                    <footer><a class="btn btn-lg btn-primary" type="button" href="/recruitment">Our process</a></footer>
                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
            </article>
        </div>
    </div>
</section>


