<section class="col-fullbleed rooster-blog white">
    <div class="col-full sourcing">
            <?php
            $args_c = array('post_type' => 'recruiting', 'name' => 'connecting');
            $my_query_c = new WP_Query($args_c);
            while ($my_query_c->have_posts()) :
                $my_query_c->the_post();
                ?>
                <div class="column-8 entry-content">
                    <h1 class="section-header orange text-center"><?php the_title(); ?></h1>
                    <?php the_content(); ?>		
                    <?php edit_post_link(__('<i class="fa fa-pencil-square-o"></i> Edit', 'upbootwp'), '<div class="btn-group edit-post">', '</div>'); ?>
                </div>
            <?php endwhile; ?>
        <div class="column-8 flexy">
            <!-- Child Connecting post / Button trigger modal -->
            <?php
            $args_cc = array(
                'post_type' => 'recruiting',
                'category_name' => 'connect',
                'orderby' => 'post_date',
                'order' => 'date',
                'post_status' => 'publish',
            );
            $my_query_cc = new WP_Query($args_cc);
            $postx_counter_cc = -1;
            if (have_posts()) :
                while ($my_query_cc->have_posts()) : $my_query_cc->the_post();
                    $postx_counter_cc ++;
                    ?>
                    <div  class="tile-cell bg" data-count="<?php echo $postx_counter_cc ?>">
                        <article id="modal-content-<?php echo $postx_counter_cc ?>" class=" mod-pop" >
                            <?php if (has_post_thumbnail()) { ?>
                                <a href="#modal" name="modal"><?php the_post_thumbnail('', array('class' => "img-responsive")); ?></a>
                            <?php } ?>
                            <a class="truncate" href="#modal" name="modal"><?php the_title(); ?></a>
                            <div class="popup-title">
                                <?php the_title(); ?>
                            </div>    
                            <div class="popup-content">
                                <?php the_content(); ?>
                            </div>
                            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                        </article>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>