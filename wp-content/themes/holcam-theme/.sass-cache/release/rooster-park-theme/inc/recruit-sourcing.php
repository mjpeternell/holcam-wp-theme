<section class="col-fullbleed rooster-blog white">
    <div class="col-full sourcing">
        <?php
        // Start the page loop.
        while (have_posts()) : the_post();
        // Include the page content template.
        //get_template_part('content', 'page');
        // End the loop.
        endwhile;
        ?>
        <?php
        $args_a = array('post_type' => 'recruiting', 'name' => 'sourcing');
        $my_query_a = new WP_Query($args_a);
        while ($my_query_a->have_posts()) :
            $my_query_a->the_post();
            ?>
            <div class="column-8 entry-content">
                <h1 class="section-header orange text-center"><?php the_title(); ?></h1>
                <?php the_content(); ?>		
                <?php edit_post_link(__('<i class="fa fa-pencil-square-o"></i> Edit', 'upbootwp'), '<div class="btn-group edit-post">', '</div>'); ?>
            </div>
        <?php endwhile; ?>	
        <!-- Child Sourcing post / Button trigger modal -->
        <div class="column-8 flexy">
            <?php
            $args_aa = array(
                'post_type' => 'recruiting',
                'category_name' => 'sourcing',
                'orderby' => 'post_date',
                'order' => 'date',
                'post_status' => 'publish',
            );
            $my_query_aa = new WP_Query($args_aa);
            $postx_counter = -1;
            if (have_posts()) :
                while ($my_query_aa->have_posts()) : $my_query_aa->the_post();
                    $postx_counter++;
                    ?>
                    <div  class="tile-cell bg" data-count="<?php echo $postx_counter; ?>">
                        <article id="modal-content-<?php echo $postx_counter; ?>" class=" mod-pop" >
                            <?php if (has_post_thumbnail()) { ?>
                                <a href="#modal" name="modal"><?php the_post_thumbnail('', array('class' => "img-responsive")); ?></a>
                            <?php } ?>
                            <a class="truncate" href="#modal" name="modal"><?php the_title(); ?></a>
                            <div class="popup-title">
                                <?php the_title(); ?>
                            </div>    
                            <div class="popup-content">
                                <?php the_content(); ?>
                            </div>
                            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                        </article>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
</section>