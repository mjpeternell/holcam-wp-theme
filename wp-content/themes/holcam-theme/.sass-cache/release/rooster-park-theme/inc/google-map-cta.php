
<?php
$gmap_cta_hero_image = get_field('gmap_cta_hero_image');
//    echo "<pre>";
//    print_r($gmap_cta_hero_image);
//    echo "</pre>";
if (!empty($gmap_cta_hero_image)):
    $size = 'hero-cta-1400';
    $url = $gmap_cta_hero_image['url'];
    $natural_width = $gmap_cta_hero_image['width'];
    $natural_height = $gmap_cta_hero_image['height'];
    $title = $gmap_cta_hero_image['title'];
    $alt = $gmap_cta_hero_image['alt'];
    $caption = $gmap_cta_hero_image['caption'];
    $resp_size_range = "(min-width: 320px) 50vw, 100vw"; //(max-width: 90.267em) 100vw, 90.267em
    ?>
    <div class="gmap-hero parallax-window" data-natural-width="<?php echo $natural_width; ?>" data-natural-height="<?php echo $natural_height; ?>" data-image-src="<?php echo $url; ?>" data-position="0px 0px" data-speed="0.2" data-bleed="0" data-parallax="scroll" alt="<?php echo $alt; ?>"></div>		
<?php endif; ?>

<section class="col-fullbleed dust gmap-cta">
    <div class="col-full capability">
        <?php
        $location = get_field('gmaps_location');
        $more_contact_info = get_field('more_contact_info');

        $myAddress = $location['address'];
        ?>
        <?php if (get_field('google_map_cta_title')): ?>
            <h1><?php the_field('google_map_cta_title'); ?></h1>
        <?php endif; ?>
        <?php if (!empty($more_contact_info)): ?>
            <div class="entry-content"><?php echo $more_contact_info; ?></div>
        <?php endif; ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2OzUsZyFKQk0vU3yt1mNjQQ7GQFUDe9o"></script>
        <?php
        if (($_SERVER['HTTP_HOST'] === 'rooster.mattpeternell.net') || $_SERVER['HTTP_HOST'] === 'roosterpark.com') {
            $minified = ".min";
        } else {
            $minified = "";
        }
        ?>
        <script src="<?php echo $themeUrl; ?>/wp-content/themes/rooster-park-theme/assets/js/google-maps<?php echo $minified; ?>.js" type="text/javascript"></script>
        <?php
        if (!empty($location)):
            ?>
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
        <?php endif; ?>
    </div>
</section>