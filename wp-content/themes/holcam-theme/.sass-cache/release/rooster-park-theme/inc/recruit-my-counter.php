<div id="myCounter" class="col-fullbleed statistical-numbers ocean">
    <div class="col-full sourcing">
        <?php
        $args_d = array('post_type' => 'recruiting', 'name' => 'in-a-nutshell');
        $my_query_d = new WP_Query($args_d);
        while ($my_query_d->have_posts()) :
            $my_query_d->the_post();
            ?>
            <div class="column-8 entry-content">
                <h2 class="text-center"><?php the_title(); ?></h2>
                <?php the_content(); ?>		
                <?php edit_post_link(__('<i class="fa fa-pencil-square-o"></i> Edit', 'upbootwp'), '<div class="btn-group edit-post">', '</div>'); ?>
            </div>
        <?php endwhile; ?>	

        <div class="column-8">
            <div id="myCountner" class="rooster-facts">
                <?php
                if (get_field('candidates_sourced')) {
                    $candidates_sourced = get_field('candidates_sourced');
                } else {
                    $candidates_sourced = "10";
                }

                if (get_field('candidates_reply')) {
                    $candidates_reply = get_field('candidates_reply');
                } else {
                    $candidates_reply = "6";
                }

                if (get_field('candidates_fit')) {
                    $candidates_fit = get_field('candidates_fit');
                } else {
                    $candidates_fit = "3";
                }

                if (get_field('candidates_sub')) {
                    $candidates_sub = get_field('candidates_sub');
                } else {
                    $candidates_sub = "1";
                }
                ?>

                <div class="column-3 fact" data-perc="<?php echo $candidates_sourced; ?>">
                    <div class="factor"></div>
                    <div class="factor-title">Candidates Sourced</div>
                </div>
                <div class="column-3 fact" data-perc="<?php echo $candidates_reply; ?>">
                    <div class="factor"></div>
                    <div class="factor-title">Candidates Reply</div>
                </div>
                <div class="column-3 fact" data-perc="<?php echo $candidates_fit; ?>">
                    <div class="factor"> </div>
                    <div class="factor-title">Candidates A Good Fit</div>
                </div>
                <div class="column-3 fact fact-last" data-perc="<?php echo $candidates_sub; ?>">
                    <div class="factor percent"> </div>
                    <div class="factor-title">Candidates Submission</div>
                </div> 
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>