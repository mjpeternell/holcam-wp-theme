<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="col-fullbleed">
                <div class="rooster-archive">
                    <div  class="single-hero">
                        <header id="singleHero" class="entry-header">
                            <h1 class="entry-title"><?php the_title(); ?></h1>
                            <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'img-circle profile-pic' ));?> 
                        </header><!-- .entry-header -->
                    </div>
                </div>
            </div>
            <div class="col-fullbleed">
                <div class="col-full rooster-archive">
                    <div class="column-10">
                        <?php while (have_posts()) : the_post(); ?>
                            <?php //panorama_content_nav('nav-above'); ?>
                            <div class="entry-content">
                                <?php the_content(); ?>
                                <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
                            </div><!-- .entry-content -->
                            <footer class="entry-meta">
                                <?php
//                                /* translators: used between list items, there is a space after the comma */
//                                $category_list = get_the_category_list(__(', ', 'panorama'));
//
//                                /* translators: used between list items, there is a space after the comma */
//                                $tag_list = get_the_tag_list('', __(', ', 'panorama'));
//
//                                if (!panorama_categorized_blog()) {
//                                    // This blog only has 1 category so we just need to worry about tags in the meta text
//                                    if ('' != $tag_list) {
//                                        $meta_text = __('This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'panorama');
//                                    } else {
//                                        $meta_text = __('Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'panorama');
//                                    }
//                                } else {
//                                    // But this blog has loads of categories so we should probably display them here
//                                    if ('' != $tag_list) {
//                                        $meta_text = __('This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'panorama');
//                                    } else {
//                                        $meta_text = __('This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'panorama');
//                                    }
//                                } // end check for categories on this blog
//                                printf(
//                                        $meta_text, $category_list, $tag_list, get_permalink(), the_title_attribute('echo=0')
//                                );
                                ?>
                                <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                            </footer><!-- .entry-meta -->
                            <?php //panorama_content_nav('nav-below'); ?>
                        <?php endwhile; // end of the loop. ?>
                    </div>
                </div>
            </div>
            <div class="col-full single-blog-nav below bot-margin">
                    <div class="column-10 text-center">
                        <?php panorama_content_nav('nav-below'); ?>
                    </div>
                </div>

        </article><!-- #post-<?php the_ID(); ?> -->
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php
get_template_part('inc/footer-cta-single');
get_footer();
?>