<?php
/**
 * Template Name: Home Page - Full Width w/ Hero
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax'); 
?>
    <?php get_template_part('inc/we-do-both'); ?>
    <?php get_template_part('inc/testimonial-loop'); ?>
    <?php get_template_part('inc/brands'); ?>
    <?php get_template_part('inc/case-study-loop'); ?>
<?php
get_template_part('inc/footer-cta');
get_footer();


