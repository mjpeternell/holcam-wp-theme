<?php
/**
 * Template Name: Careers Page - Careers
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
//get_template_part('inc/hero');
get_template_part('inc/hero-parallax');
?>            
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="col-fullbleed grey">
            <div class="col-full">
                <section id="careers" class="">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content', 'page'); ?>
                        <?php //comments_template('', true); ?>
                    <?php endwhile; // end of the loop. ?>         
                </section>                                    </div>
        </div>
        <div class="col-fullbleed white">
            <div class="col-full">
                <section id="careers-posts">
                    <?php
                    $accesstoken = "c3ceb49bda5d2ddb6563029f4bd2c4f8";
                    // init curl object        
                    $ch = curl_init();
                    $headr = array();
                    $headr[] = 'Content-length: 0';
                    $headr[] = 'Content-type: application/json';
                    $headr[] = 'Authorization: Token ' . $accesstoken;

                    // define options
                    $optArray = array(
                        CURLOPT_HTTPHEADER => $headr,
                        CURLOPT_URL => "https://api.catsone.com/v3/jobs?per_page=100",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 1000,
                    );

                    // apply those options
                    curl_setopt_array($ch, $optArray);

                    // execute request and get response
                    $result = curl_exec($ch);
                    //var_dump(json_decode($result));                       
                    $result_array = (json_decode($result, true));
                    ?>
                    <div class="job-list-container">
                        <div class="job-list">
                            <div class="job-header">
                                <div class="th-1"><span>Job Title</span></div>
                                <div class="th-2"><span>Location</span></div>
                            </div>
                            <div class="job-postings">
                                <?php
                                $count = -1;
                                foreach ($result_array['_embedded']['jobs'] as $value) {
                                    //[company_id] => 2608289
                                    $even = "even";
                                    $odd = "odd";
                                    $ulink = "";
                                    $status = "";
                                    $coid = "";
                                    $short_desc = $value['_embedded']['custom_fields'][1]['value'];

                                    if ($value['company_id'] == "14799625") {
                                        $coid = "Job Posting";
                                    }

                                    if ($value['id']) {
                                        $ulink = $value['id'];
                                    }


                                    //if ($value['_embedded']['status']['id'] == "226209") {
                                    if ($coid) {
                                        $count++;
                                        echo '<div class="job-post ' . (($count % 2) == 0 ? $odd : $even) . ' " data-count="' . $count . '">';
                                        echo '<div class="link">';
                                        echo '<a href="http://roosterpark.catsone.com/careers/index.php?a=details&jobOrderID=' . $ulink . '" title="' . $value['title'] . '" target="_blank">' . $value['title'] . '</a>';
                                        echo '</div>';
                                        echo '<div class="local">' . $value['location']['city'] . ', ' . $value['location']['state'] . '</div>';
                                        echo '<div class="description">';
                                        //echo '<p>' . $value['_embedded']['status']['title'] . ' - ' . $coid . '</p>';
                                        echo '<p>' . $short_desc ? : $short_desc . '</p>';
                                        echo '</div>';


                                        //
                                        //echo '<td><a href="http://roosterpark.catsone.com/careers/index.php?a=details&jobOrderID='. $ulink.'" title="'. $value['title'].'" target="_blank">' . $value['title'] . '</td><td>' . $value['location']['city'] . ', ' . $value['location']['state'] . '</td><td>' . $value['_embedded']['status']['title'] . '</td><td>' . $coid . '</td>';
                                        //echo '<td><a href="http://roosterpark.catsone.com/careers/index.php?a=details&jobOrderID='. $ulink.'" title="'. $value['title'].'" target="_blank">' . $value['title'] . '</td><td>' . $value['location']['city'] . ', ' . $value['location']['state'] . '</td>';
                                        echo '</div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    //echo '<pre style="font-size: 10px;">';
                    //print_r($result_array);
                    //echo "</pre>";
                    // also get the error and response code
                    //$errors = curl_error($ch);
                    //$response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    curl_close($ch);

                    //var_dump($errors);
                    //var_dump($response);
                    ?>
                </section>
            </div>
        </div>

    </main><!-- .site-main -->
</div><!-- .content-area -->  
<?php
//get_template_part('inc/footer-cta');
get_footer();


