<?php
/**
 * Template Name: Blog Page - Rooster Blog
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax');
//get_template_part('inc/hero'); 
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main white" role="main">
        <div class="col-fullbleed rooster-blog">
            <div class="col-full">


                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                // Include the page content template.
                //get_template_part('content', 'page');
                // End the loop.
                endwhile;
                ?>

                <section id="Rooster-Blog" class="row flexy">

                    <?php
                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $blog_arg = array(
                        'post_type' => 'rooster_blog',
                        'orderby' => 'post_date',
                        'order' => 'date',
                        'post_status' => 'publish',
                        'posts_per_page' => 12,
                        'paged' => $paged,
                    );
                    $wp_blog_query = new WP_Query($blog_arg);
                    $postx_counter = -1;
                    if (have_posts()) :
                        while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                            $postx_counter++;
                            $offset = "";
                            if ($postx_counter == 0) {
                                $offset = "col-lg-offset-2";
                            } else {
                                $offset = "col-lg-offset-1";
                            }
                            ?>
                            <div class="tile bg" data-count="<?php echo $postx_counter; ?>">
                                <article class="tile-inner" >
                                    <header class="entry-header"> <h1 class="title"><a class="truncate" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></header>
                                    <div class="blog-content">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <footer class="entry-meta">
                                        <a class="btn btn-link blog-link" href="<?php the_permalink(); ?>" role="button">Read More</a>
                                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '', '', 0, 'post-edit-link btn btn-default'); ?>
                                    </footer>
                                </article>
                            </div>
                        <?php endwhile;
                        ?>
                    </section>  
                    <?php if ($wp_blog_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                        <section>

                            <nav class="post-navigation col-lg-12">
                                <div class="nav-previous">
                                    <?php echo get_next_posts_link('<i class="fa fa-angle-left" aria-hidden="true"></i> Older Entries', $wp_blog_query->max_num_pages); // display older posts link  ?>
                                </div>
                                <div class="nav-next">
                                    <?php echo get_previous_posts_link('Newer Entries <i class="fa fa-angle-right" aria-hidden="true"></i>'); // display newer posts link  ?>
                                </div>
                            </nav>

                        </section>
                        <?php
                    }
                endif;
                ?>
                <?php wp_reset_postdata(); ?>


            </div>
    </main><!-- .site-main -->
</div>
<?php
//get_template_part('inc/footer-cta');
get_footer();
?>
