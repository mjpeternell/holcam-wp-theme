<?php
/**
 * Template Name: Capabilities Page
 *
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax');
//get_template_part('inc/hero'); 
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('inc/capabilities-recruitment'); ?>
        <?php get_template_part('inc/capabilities-dev-services'); ?>
        <?php //get_template_part('inc/capability-cta'); ?>
        <?php get_template_part('inc/capabilities-case-studies'); ?>
        <?php //get_template_part('inc/modal-popup'); ?>
        <?php //get_template_part('inc/footer-cta'); ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->    
<?php get_footer(); ?>
