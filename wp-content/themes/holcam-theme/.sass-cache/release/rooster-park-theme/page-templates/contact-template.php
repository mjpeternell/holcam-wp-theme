<?php
/**
 * Template Name: Contact Us Page
 *
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax');
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php while (have_posts()) : the_post(); ?>
            <section class="col-fullbleed contact-content">
                <div class="col-full">
                    <div class="col-lg-8 entry-content">
                        <?php the_content(); ?>     
                    </div>
                </div>
            </section>
        <?php endwhile; // end of the loop. ?>
        <?php get_template_part('inc/google-map-cta'); ?>
        <?php //get_template_part('inc/footer-cta'); ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->    
<?php get_footer(); ?>
