<?php

/**
 * RoosterPark functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
// Useful global constants
define('ROOSTER_PARK_VERSION', '0.1.0');

/**
 * Add humans.txt to the <head> element.
 */
function rooster_park_header_meta() {
    $humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';

    echo apply_filters('rooster_park_humans', $humans);
}

add_action('wp_head', 'rooster_park_header_meta');

/**
 * Set the theme's image size.
 *
 * @since Panorama 1.0
 */
add_image_size('hero-cta-480', 480, 350, array('left', 'top'));
add_image_size('hero-cta-680', 680, 350, array('left', 'top'));
add_image_size('hero-cta-1024', 1024, 256, array('left', 'top'));
add_image_size('hero-cta-1400', 1400, 350, array('left', 'top'));

add_image_size('instagram-square', 320, 320, array('left', 'top'));
add_image_size('hero-img-xxs', 320, 320, array('left', 'top'));
add_image_size('hero-img-sm', 480, 480, array('left', 'top'));
add_image_size('hero-img-medium-a', 680, 680, array('left', 'top'));
add_image_size('hero-img-medium-b', 768);
add_image_size('hero-img-large-a', 992);
add_image_size('hero-img-large-b', 1200);
add_image_size('hero-imag-xlarge', 1354);


add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3);

function remove_thumbnail_dimensions($html, $post_id, $post_image_id) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Panorama 1.0
 */
if (!isset($content_width))
    $content_width = 640; /* pixels */

if (!function_exists('rooster_park_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * @since Panorama 1.0
     */
    function rooster_park_setup() {
        /**
         * Custom template tags for this theme.
         */
        require 'inc/navwalker.php';

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'panorama'),
            'footer' => __('Footer Links', 'panorama') // secondary nav in footer
        ));

        /**
         * Custom template tags for this theme.
         */
        require( get_template_directory() . '/inc/template-tags.php' );

        /**
         * Custom functions that act independently of the theme templates
         */
        require( get_template_directory() . '/inc/extras.php' );

        /**
         * Customizer additions
         */
        require( get_template_directory() . '/inc/customizer.php' );

        /**
         * WordPress.com-specific functions and definitions
         */
        //require( get_template_directory() . '/inc/wpcom.php' );

        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain('panorama', get_template_directory() . '/languages');

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support('post-thumbnails');

        function custom_admin_head() {
            $css = '';
            $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
            echo '<style type="text/css">' . $css . '</style>';
        }

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'panorama'),
        ));

        /**
         * Add support for the Aside Post Formats
         */
        add_theme_support('post-formats', array('aside',));
    }

endif; // rooster_park_setup
add_action('after_setup_theme', 'rooster_park_setup');

/**
 * 
 * Add SVG Support to media library
 * 
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since Panorama 1.0
 */
function rooster_park_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'panorama'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
}

add_action('widgets_init', 'rooster_park_widgets_init');

/**
 * Enqueue scripts and styles
 */
function rooster_park_scripts() {
    /**
     * Check for Dev Environment if true load unminified scripts and css else load minified versions.
     */
    $minified = "";
    //echo $myDomain = $_SERVER['HTTP_HOST'];

    if (($_SERVER['HTTP_HOST'] === 'panorama.mattpeternell.net') || $_SERVER['HTTP_HOST'] === 'roosterpark.com') {
        $minified = ".min";
    } else {
        $minified = "";
    }
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('rooster_park_sass', get_template_directory_uri() . '/assets/css/theme-style' . $minified . '.css', array(), ROOSTER_PARK_VERSION);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.1.min.js', array('jquery'), '20120206', true);
    wp_enqueue_script('parallax-js', get_template_directory_uri() . '/assets/js/vendor/parallax.min.js', array('jquery'), '20120206', true);
    wp_enqueue_script('modernizer', get_template_directory_uri() . '/assets/js/vendor/modernizr.min.js', array(), '3.3.1', true);
    wp_enqueue_script('panorama', get_template_directory_uri() . '/assets/js/roosterpark' . $minified . '.js', array(), ROOSTER_PARK_VERSION, true);
    wp_enqueue_script('succinct-truncate', get_template_directory_uri() . "/assets/js/vendor/jQuery.succinct.min.js", array(), ROOSTER_PARK_VERSION, true);
    wp_enqueue_script('rooster_park_extras', get_template_directory_uri() . '/assets/js/roosterpark-plugins' . $minified . '.js', array(), ROOSTER_PARK_VERSION, true);
    wp_enqueue_script('count-up', get_template_directory_uri() . '/assets/js/vendor/count.js', array('jquery'), '20120206', true);
    wp_enqueue_script('inview-js', get_template_directory_uri() . '/assets/js/vendor/jquery.inview.min.js', array('jquery'), '20120206', true);


    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (is_singular() && wp_attachment_is_image()) {
        wp_enqueue_script('keyboard-image-navigation', get_template_directory_uri() . '/assets/js/vendor/keyboard-image-navigation.js', array('jquery'), '20120202');
    }
}

add_action('wp_enqueue_scripts', 'rooster_park_scripts');

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );


/**
 * Implement ACF Google Maps Key
 */
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyD2OzUsZyFKQk0vU3yt1mNjQQ7GQFUDe9o');
}
add_action('acf/init', 'my_acf_init');

/**
 * Implement ACF Google Maps Key
 */

function gp_register_taxonomy_for_object_type() {
    register_taxonomy_for_object_type('post_tag', 'rooster_blog');
}
add_action('init', 'gp_register_taxonomy_for_object_type');


function gp_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'nav_menu_item','rooster_blog'
		));
	  return $query;
	}
}
add_filter( 'pre_get_posts', 'gp_add_custom_types' );
