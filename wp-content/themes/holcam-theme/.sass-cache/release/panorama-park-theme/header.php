
<?php
/**
 * The template for displaying the header.
 *
 * @package Panorama
 * @since 0.1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php if (is_singular() && pings_open(get_queried_object())) : ?>
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php endif; ?>
        <?php wp_head(); ?>
        <!--[if lt IE 9]>
            <script src="<?php echo get_stylesheet_directory_uri() . "/assets/js/vendor/html5shiv.js"; ?>"></script>
            <script src="<?php echo get_stylesheet_directory_uri() . "/assets/js/vendor/respond.min.js"; ?>"></script>
        <![endif]--> 
        <?php $themeUrl = get_stylesheet_directory_uri(); ?>

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo $themeUrl; ?>/assets/favicons/manifest.json">
        <link rel="mask-icon" href="<?php echo $themeUrl; ?>/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="<?php echo $themeUrl; ?>/assets/favicons/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">

    </head>
    <body <?php body_class(); ?>>
         
        <header class="navigation " role="banner" id="Header">
            
            <div class="navigation-wrapper">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="logo"  rel="home">
                    <?php get_template_part('inc/panorama-logo')//header_image(); ?>
                </a>
                <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">MENU</a>
                <?php
                $args = array(
                    'theme_location' => 'primary',
                    'container' => 'nav',
                    'container_id' => 'navbar',
                    'container_class' => 'navbar-collapse collapse',
                    'menu_class' => 'navigation-menu',
                    'fallback_cb' => '',
                    'menu_id' => 'js-navigation-menu',
                    'walker' => new rooster_park_navwalker()
                );
                wp_nav_menu($args);
                ?>
            </div>
        </header>
        <div id="page" class="hfeed site">
            <div id="main" class="site-main">
