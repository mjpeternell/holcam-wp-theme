<section id="capabilitiesCaseStudies" class="col-fullbleed ocean capCaseStud">
    <div class="col-full">
        <h1 class="section-header orange">Case Studies</h1>
    </div>
    <div class="col-full">
        <?php
        $ccs_arg = array(
            'post_type' => 'case_studies',
            'posts_per_page' => 6,
            'orderby' => 'post_date',
            'order' => 'date',
            'post_status' => 'publish',
        );
        $wp_ccs_query = new WP_Query($ccs_arg);
        $post_ccs_counter = -1;
        ?>
        <div class="cs-logo-list">
            <?php
            while ($wp_ccs_query->have_posts()) : $wp_ccs_query->the_post();
                $post_ccs_counter++;
                ?>
                <div class="cs-logo-list-item" data-count="<?php echo $post_ccs_counter; ?>">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('', array('class' => 'cs-logo-pic')); ?></a>
                </div>
            <?php endwhile; ?>
        </div>
        <?php wp_reset_postdata(); ?>
    </div>
</section>