<?php
/*
 * Hero Image: includes ACF Hero image, title, subtitle and copy.
 * Pages: Homepage,
 */
?>
<?php
$hero_alt = "";
if (!is_front_page()) {
    $hero_alt = "hero-alt";
} else {
    $hero_alt = "hero-home";
}

$hero_height = get_field('hero_height');

$hero_height_val = "";
if ($hero_height) {
    $hero_height_val = $hero_height;
}

$image = get_field('hero_image');

if (!empty($image)):
    $size = 'hero-imag-xlarge';
    $parallax = $image['sizes'][$size];
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];
    $resp_size_range_hero = "(min-width: 320px) 50vw, 100vw";
    ?>

    <style type="text/css">
        .parallax-background {
            background: url("<?php echo $url; ?>") repeat;
        }
    </style>
<?php endif; ?>
<section class="col-fullbleed aluminium hero">
    <div class="col-full">
        <div id="js-parallax-window" class="parallax-window-2 <?php echo $hero_height_val; ?>">
            <div class="parallax-static-content">
                <?php
                echo '<div class="caption">';
                if (get_field('hero_title')) {
                    echo '<h1><span>' . get_field('hero_title') . '</span></h1	>';
                }
                echo '</div>';
                ?>
            </div>
            <div id="js-parallax-background" class="parallax-background" ></div>
        </div>

    </div>
</section>