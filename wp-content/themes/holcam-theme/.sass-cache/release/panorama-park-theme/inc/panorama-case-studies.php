<?php
/*
 * Case Study Loop: includes ACF, title, copy and link.
 * Pages: Homepage,
 */
?>
<section id="CaseStudiesHome" class="col-fullbleed white three-col-section case-studies">
    <div class="col-full">
        <div class="column-10  offset-1">
        <?php get_template_part('template-parts/acf','repeater'); ?>
        </div>
    </div>
</section>