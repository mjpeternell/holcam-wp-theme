<?php

/**
 * Template Name: Contact Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax');
?>
<?php //get_template_part('inc/page-template-intro-content'); ?>
<?php get_template_part('inc/page-template-flex-content'); ?>
<?php

get_footer();


