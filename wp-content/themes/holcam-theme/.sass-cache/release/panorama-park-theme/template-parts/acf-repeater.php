<?php if (have_rows('cta_repeater', 'option')): ?>
    <?php
    while (have_rows('cta_repeater', 'option')): the_row();
        // vars
        $image = get_sub_field('cta_icon', 'option');
        $title = get_sub_field('cta_title', 'option');
        $excerpt = get_sub_field('cta_excerpt', 'option');
        $link = get_sub_field('cta_link', 'option');
        $link_label = get_sub_field('cta_link_label', 'option');
        ?>

        <div class="column-4 align-center">
            <?php if ($image): ?>
                <div class="featured-icon">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                </div>
            <?php endif; ?>
            <?php if ($title): ?>
                <h3 class="content-box-heading"><?php echo $title; ?></h3>
            <?php endif; ?>
            <?php if ($excerpt): ?>
                <div class="content-container">
                    <?php echo $excerpt; ?>
                </div>
            <?php endif; ?>
            <?php if ($link): ?>
                <a class="cs-link" href="<?php echo $link; ?>"><?php echo $link_label; ?></a>
            <?php endif; ?>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

