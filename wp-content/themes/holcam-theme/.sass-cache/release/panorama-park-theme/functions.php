<?php

/**
 * Panorama functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
// Useful global constants
define('PANORAMA_VERSION', '0.1.0');

/**
 * Add humans.txt to the <head> element.
 */
function panorama_header_meta() {
    $humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';

    echo apply_filters('panorama_humans', $humans);
}

add_action('wp_head', 'panorama_header_meta');

/**
 * Set the theme's image size.
 *
 * @since Panorama 1.0
 */
add_image_size('hero-cta-480', 480, 292, array('left', 'top'));
add_image_size('hero-cta-680', 680, 292, array('left', 'top'));
add_image_size('hero-cta-1024', 1024, 864, array('left', 'top'));
add_image_size('hero-cta-1280', 1280, 1080, array('left', 'top'));
add_image_size('instagram-square', 480, 480, array('left', 'top'));
add_image_size('instagram-square-a', 680, 680, array('left', 'top'));
add_image_size('hero-img-medium-b', 768);
add_image_size('hero-img-large-a', 992);
add_image_size('hero-img-large-b', 1200);
add_image_size('hero-imag-xlarge', 1354);


add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3);

function remove_thumbnail_dimensions($html, $post_id, $post_image_id) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Panorama 1.0
 */
if (!isset($content_width))
    $content_width = 640; /* pixels */

if (!function_exists('panorama_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * @since Panorama 1.0
     */
    function panorama_setup() {
        /**
         * Custom template tags for this theme.
         */
        require 'inc/navwalker.php';

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'panorama'),
            'footer' => __('Footer Links', 'panorama') // secondary nav in footer
        ));

        /**
         * Custom template tags for this theme.
         */
        require( get_template_directory() . '/inc/template-tags.php' );

        /**
         * Custom functions that act independently of the theme templates
         */
        require( get_template_directory() . '/inc/extras.php' );

        /**
         * Customizer additions
         */
        require( get_template_directory() . '/inc/customizer.php' );


        /**
         * Add Thumbnails in Manage Posts/Pages List
         */
        require( get_template_directory() . '/inc/admin-feature-image.php' );

        /**
         * ACF Options Page Setup
         */
        require( get_template_directory() . '/inc/admin-acf-options-page.php' );

        /**
         * Add SVG Support to media library
         */
        require( get_template_directory() . '/inc/admin-svg-suport-media.php' );
        
        /**
         * WordPress.com-specific functions and definitions
         */
        //require( get_template_directory() . '/inc/wpcom.php' );

        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain('panorama', get_template_directory() . '/languages');

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support('post-thumbnails');

        function custom_admin_head() {
            $css = '';
            $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
            echo '<style type="text/css">' . $css . '</style>';
        }

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'panorama'),
        ));

        /**
         * Add support for the Aside Post Formats
         */
        add_theme_support('post-formats', array('aside',));
    }

endif; // panorama_setup
add_action('after_setup_theme', 'panorama_setup');

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since Panorama 1.0
 */
function panorama_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'panorama'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
}

add_action('widgets_init', 'panorama_widgets_init');

/**
 * Enqueue scripts and styles
 */
function panorama_scripts() {
    /**
     * Check for Dev Environment if true load unminified scripts and css else load minified versions.
     */
    $minified = "";
    //echo $myDomain = $_SERVER['HTTP_HOST'];

    if (($_SERVER['HTTP_HOST'] === 'panorama.mattpeternell.net') || $_SERVER['HTTP_HOST'] === 'panoramaglobal.org') {
        $minified = ".min";
    } else {
        $minified = "";
    }

    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('panorama_sass', get_template_directory_uri() . '/assets/css/theme-style' . $minified . '.css', array(), PANORAMA_VERSION);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.1.min.js', array('jquery'), '20120206', true);
    wp_enqueue_script('parallax-js', get_template_directory_uri() . '/assets/js/vendor/parallax.min.js', array('jquery'), '20120206', true);
    wp_enqueue_script('modernizer', get_template_directory_uri() . '/assets/js/vendor/modernizr.min.js', array(), '3.3.1', true);
    wp_enqueue_script('panorama', get_template_directory_uri() . '/assets/js/panorama' . $minified . '.js', array(), PANORAMA_VERSION, true);
    wp_enqueue_script('succinct-truncate', get_template_directory_uri() . "/assets/js/vendor/jQuery.succinct.min.js", array(), PANORAMA_VERSION, true);
    wp_enqueue_script('panorama_extras', get_template_directory_uri() . '/assets/js/panorama-plugins' . $minified . '.js', array(), PANORAMA_VERSION, true);
    wp_enqueue_script('count-up', get_template_directory_uri() . '/assets/js/vendor/count.js', array('jquery'), '20120206', true);
    wp_enqueue_script('inview-js', get_template_directory_uri() . '/assets/js/vendor/jquery.inview.min.js', array('jquery'), '20120206', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (is_singular() && wp_attachment_is_image()) {
        wp_enqueue_script('keyboard-image-navigation', get_template_directory_uri() . '/assets/js/vendor/keyboard-image-navigation.js', array('jquery'), '20120202');
    }
}

add_action('wp_enqueue_scripts', 'panorama_scripts');

/**
 * Set CSS Color class in <body> tag using ACF
 */
require( get_template_directory() . '/inc/admin-acf-body-class.php' );
