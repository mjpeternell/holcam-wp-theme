/*! Panorama - panorama.js - v0.1.0 - 2016-04-01
 * http://panoramaDotcom
 * Copyrigh0;
 jQuery(document).ready(function () {t (c) 2016; * Licensed GPLv2+ */

jQuery(document).ready(function () {
    
    //Check current widow scroll position and show/hide cloned nav menu.
    jQuery(window).scroll(function () {
        var scrollPos = jQuery(document).scrollTop();
        //console.log(scrollPos);
        if (scrollPos <= 374) {
            jQuery('.navigation.banner--clone').slideUp("fast");
        }
        if (scrollPos >= 375) {
            jQuery('.navigation.banner--clone').slideDown("fast");
            console.log(scrollPos);
        }
    });

    //Resize Hero Window
    function sliderResize() {
        //My Window Height detections
        var myWindow_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var myWindow_h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        var myHero = jQuery('#js-parallax-window.tall');
        var myHero_short = jQuery('#js-parallax-window.short');
        var myHeroAlt = jQuery('#hero.hero-alt');
        var singleHero = jQuery('#singleHero');

        if (myWindow_w <= 480) {
            console.log("480 screen - H: " + myWindow_h + " W: " + myWindow_w);
            myHero.css({height: 480, width: 'inherit'});
            myHero_short.css({height: 450, width: "auto"});
            singleHero.css({height: 450, width: "auto"});
        } else if (myWindow_w <= 800) {
            console.log("800 screen - H: " + myWindow_h + " W: " + myWindow_w);
            myHero.css({height: 480, width: 'inherit'});
            myHeroAlt.css({height: 450, width: "auto"});
            singleHero.css({height: 450, width: "auto"});
        } else if (myWindow_w <= 1024) {
            console.log("1024 screen - H: " + myWindow_h + " W: " + myWindow_w);
            myHero.css({height: (myWindow_h - 175), width: 'inherit'});
            myHero_short.css({height: 500, width: "auto"});
            singleHero.css({height: myWindow_h * 0.65, width: "auto"});
        } else if (myWindow_w <= 1280) {
            console.log("1280 screen - H: " + myWindow_h + " W: " + myWindow_w);
            myHero.css({height: 760, width: "auto"});
            myHero_short.css({height: 500, width: "auto"});
            singleHero.css({height: myWindow_h * 0.65, width: "auto"});
        } else {
            console.log("else H: " + myWindow_h + " W: " + myWindow_w);
            myHero.css({height: 760, width: "auto"});
            myHero_short.css({height: 500, width: "auto"});
            singleHero.css({height: (myWindow_h - 400), width: "auto"});
        }
    }

    jQuery(window).resize(function () {
        sliderResize();
    });

    jQuery(window).load(function () {
        sliderResize();
    });

    // Listen for orientation changes
    window.addEventListener("orientationchange", function () {
        sliderResize();
    }, false);

    /** In View https://github.com/protonet/jquery.inview **/
    jQuery('#myCounter').on('inview', function (event, isInView) {
        if (isInView) {
            //console.log("Counter in view");
            /* === COUNT FACTORS - https://github.com/mhuggins/jquery-countTo === */
            var dataperc;
            jQuery('.fact').each(function () {
                dataperc = jQuery(this).attr('data-perc'),
                        jQuery(this).find('.factor').delay(10000).countTo({
                    from: 0,
                    to: dataperc,
                    speed: 3000,
                    refreshInterval: 50
                });
            });
        } else {
            //console.log("Counter Not In View");
        }
    });

    //truncate stings    
    jQuery('#holcam-blog .blog-content p').succinct({
        size: 190
    });

    jQuery('#CaseStudiesHome .content-container p').succinct({
        size: 80
    });

    //scroll to top functionality
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-to-top').fadeIn();
        } else {
            jQuery('.scroll-to-top').fadeOut();
        }
    });

    //Click event to scroll to top
    jQuery('.scroll-to-top').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    window.onorientationchange = function () {
        window.location.reload();
    };

//triggered when modal is about to be shown
    jQuery('.mod-pop button').on('click', function (e) {
        //get data-id attribute of the clicked element

        //populate the textbox

    });

    jQuery('a[name=\'modal\']').click(function (e) {

        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = jQuery(this).attr('href');

        //Get the window height and width
        var winH = jQuery(window).height();
        var winW = jQuery(window).width();
        console.log(winH + ' - ' + winW);
        //Set the popup window to center
        if (winW <= 680) {
            jQuery(id).css('top', 40);
        } else {
            jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        }
        //jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        jQuery(id).css('left', winW / 2 - jQuery(id).width() / 2);

        //var theTitle = jQuery(this).siblings(".entry-header").children('h1').find('a').text();
        var theTitle = jQuery(this).siblings('.popup-title').html();
        var theContent = jQuery(this).siblings(".popup-content").html();
        console.log(theTitle);
        console.log(theContent);
        jQuery(id).find('header h2').append(theTitle);
        jQuery(id).find('.content').append(theContent);

        //transition effect
        jQuery('#modal_screen').fadeIn(500);
        jQuery(id).fadeIn(500);

    });

//if close button is clicked
    jQuery('.modalwindow .close-btn').click(function (e) {
        console.log(e);
        var mod_id = jQuery('.modalwindow');
        jQuery('.modalwindow').find('header h2').empty();
        jQuery('.modalwindow').find('.content p').remove();
        //Cancel the link behavior
        e.preventDefault();

        jQuery('.modalwindow').fadeOut(500);
        jQuery('#modal_screen').fadeOut(500);
    });

    jQuery(window).resize(function () {
        var more = document.getElementById("js-navigation-more");
        if (jQuery(more).length > 0) {
            var windowWidth = $(window).width();
            var moreLeftSideToPageLeftSide = jQuery(more).offset().left;
            var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;

            if (moreLeftSideToPageRightSide < 330) {
                jQuery("#js-navigation-more .submenu .submenu").removeClass("fly-out-right");
                jQuery("#js-navigation-more .submenu .submenu").addClass("fly-out-left");
            }

            if (moreLeftSideToPageRightSide > 330) {
                jQuery("#js-navigation-more .submenu .submenu").removeClass("fly-out-left");
                jQuery("#js-navigation-more .submenu .submenu").addClass("fly-out-right");
            }
        }
    });


    var menuToggle = jQuery("#js-mobile-menu").unbind();

    menuToggle.on("click", function (e) {
        e.preventDefault();
        jQuery("#js-navigation-menu").slideToggle(function () {
            jQuery("#js-navigation-menu").toggleClass("show");
        });
    });


});