<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="column-10 offset-1 my-error-404">
                <article id="post-0" class="post not-found">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php _e('Oops! That page can&rsquo;t be found.', 'panorama'); ?></h1>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <p><?php _e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'panorama'); ?></p>

                        <?php get_search_form(); ?>

                    </div><!-- .entry-content -->
                </article><!-- #post-0 .post .error404 .not-found -->
            </div>
        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php get_footer(); ?>
