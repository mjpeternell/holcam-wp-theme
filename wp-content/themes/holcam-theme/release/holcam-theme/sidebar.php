<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<div id="secondary" class="widget-area sidebar" role="complementary">
    <?php do_action('before_sidebar'); ?>
    <?php if (!dynamic_sidebar('sidebar-1')) : ?>

    <?php endif; // end sidebar widget area ?>
</div><!-- #secondary .widget-area -->
