<section id="documentation" class="column-10 offset-1 single-team-post">
    <h1 class="section-title">Documentation</h1>
    <?php if (get_field('document_description', 'option')) { ?>
        <p class="lead"><?php the_field('document_description', 'option'); ?></p>
    <?php } ?>
        <ul class="download-links">
                        <?php
                        while (have_rows('download_links')): the_row();
                            $link_url = get_sub_field('link_url');
                            if ($link_url) {
                                $my_id2 = $link_url['ID'];
                                $url_file = $link_url['url'];
                                $title_file = $link_url['title'];
                                $caption_file = $link_url['caption'];
                                $desc = $link_url['description'];
                                $icon = $link_url['icon'];
                            }
                            ?>
                            <li class="link-item ">
                                <div class="column-1">
                                    <?php if (wp_get_attachment_image($link_url['ID'])) { ?>
                                        <?php echo wp_get_attachment_image($link_url['ID'], 'thumbnail', "", array("class" => "img-thumbnail")); ?>
                                    <?php } else { ?>
                                        <img src="<?php echo $icon; ?>" class="img-responsive"/>
                                    <?php } ?>
                                </div> 
                                <div class="column-10">
                                    <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>"><?php echo $title_file; ?> <i class="fa fa-download" aria-hidden="true"></i></a>
                                    <div class="descrition"><?php echo $desc; ?></div>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
</section>