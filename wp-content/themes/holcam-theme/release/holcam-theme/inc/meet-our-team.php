<section class="col-fullbleed white meet-our-team">
    <div class="col-full">
        <h1 class="align-center">Meet Our Team</h1>
        <div id="holcam-blog" class="row flexy max">
            <!-- Button trigger modal -->

            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $blog_arg = array(
                'post_type' => 'staff',
                'orderby' => 'post_date',
                'order' => 'date',
                'post_status' => 'publish',
                'posts_per_page' => 12,
                'paged' => $paged,
            );
            $wp_blog_query = new WP_Query($blog_arg);
            $postx_counter = -1;
            if (have_posts()) :
                while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                    $postx_counter++;
                    $offset = "";
                    if ($postx_counter == 0) {
                        $offset = "col-lg-offset-2";
                    } else {
                        $offset = "col-lg-offset-1";
                    }
                    ?>
                    <div  class="tile-cell staff" data-count="<?php echo $postx_counter; ?>">
                        <article id="modal-content-<?php echo $postx_counter; ?>" class="" >
                            <?php if (has_post_thumbnail()) { ?>
                                <?php the_post_thumbnail('', array('class' => "tile-img img-responsive")); ?>
                            <?php } else { ?>
                                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/gravitar.png'; ?>" class="tile-img img-responsive"/>
                            <?php } ?>
                            <div class="popup-title">
                                <?php the_title(); ?>
                            </div>
                            <div class="popup-job-title">
                                <?php $job_title = get_field('job_title'); ?>
                                <?php if (!empty($job_title)): ?>
                                    <?php echo $job_title; ?>
                                <?php endif; ?>
                            </div>  
<!--                            <div class="popup-content">
                                <?php //the_content(); ?>
                            </div>-->
                            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                        </article>
                    </div>
                <?php endwhile; ?>
            </div>  
            <?php if ($wp_blog_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                <div>
                    <div class="row">
                        <nav class="post-navigation col-lg-12">
                            <div class="nav-previous">
                                <?php echo get_next_posts_link('<i class="fa fa-angle-double-left" aria-hidden="true"></i> Older Entries', $wp_blog_query->max_num_pages); // display older posts link  ?>
                            </div>
                            <div class="nav-next">
                                <?php echo get_previous_posts_link('Newer Entries <i class="fa fa-angle-double-right" aria-hidden="true"></i>'); // display newer posts link  ?>
                            </div>
                        </nav>
                    </div>
                </div>
                <?php
            }
        endif;
        ?>
        <?php wp_reset_postdata(); ?>
    </div>
</section>


