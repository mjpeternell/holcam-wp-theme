<section id="collections-cta" class="col-fullbleed white three-col-section collections">
    <div class="column-10  offset-1">
        <?php
        $section_title_col = get_field('section_title_col');
        ?>
        <?php if ($section_title_col): ?>
            <h2 class="section-header white align-center"><?php echo $section_title_col; ?></h2>
        <?php endif; ?>
        <?php if (have_rows('collections_cta_repeater')): ?>
            <div class="row">
                <?php
                while (have_rows('collections_cta_repeater')): the_row();
                    // vars
                    $image = get_sub_field('cta_icon');
                    // print_r($image);
                    $title = get_sub_field('cta_title');
                    $excerpt = get_sub_field('cta_excerpt');
                    $link = get_sub_field('cta_link');
                    $link_label = get_sub_field('cta_link_label');

                    // thumbnail
                    $size = 'thumbnail';
                    $thumb = $image['sizes'][$size];
                    $width = $image['sizes'][$size . '-width'];
                    $height = $image['sizes'][$size . '-height'];
                    ?>

                    <div class="column-4 align-center">
                        <?php if ($image): ?>
                            <div class="featured-icon">
                                <a href="<?php echo $link; ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                            </div>
                        <?php endif; ?>
                        <?php if ($title): ?>
                            <h3 class="content-box-heading"><?php echo $title; ?></h3>
                        <?php endif; ?>
                        <?php if ($excerpt): ?>
                            <div class="content-container">
                                <?php echo $excerpt; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($link): ?>
                            <a class="cs-link" href="<?php echo $link; ?>"><?php echo $link_label; ?></a>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>


    </div>

</section>