<section id="Cool-Stuff" class="col-fullbleed where-buy-cta">
    <div class="column-10 offset-1 text-center"> 
        <?php
        $buy_cta_title = get_field('buy_cta_title', 'option');
        $buy_cta_subtitle = get_field('buy_cta_subtitle', 'option');
        ?>
        <?php
        if ($buy_cta_title) {
            echo '<h2 class="section-header white align-center">' . $buy_cta_title . '</h2>';
        }
        if ($buy_cta_subtitle) {
            echo '<p class="lead">' . $buy_cta_subtitle . '</p>';
        }
        ?>
        <form  class="form-inline" role="search" id="BridgerForm" name="BridgerForm" method="post" action="/dealer-locator" onsubmit="return validateZip(this);">
            <input name="divisionCode" value="H" type="hidden">
            <input value="N5" name="Search_Param" type="hidden">
            <input class="form-control" value="" onfocus="clearInputForm(this);" name="postalcode" id="zipSearch" placeholder="Enter ZIP Code" type="text">
            <input class="search" id="searchsubmit" value="Search" type="submit">
        </form>
    </div>
</section>