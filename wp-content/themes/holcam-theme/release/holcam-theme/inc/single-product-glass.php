<section id="glassTypes" class="column-10 offset-1 single-team-post">
    <?php
// URL Parameters to create return link on archive page
    $get_pt = get_post_type();
    $get_slug = get_post_field('post_name', get_post());


//ACF Sets Product Options filtering flag

    if (get_field('collection_flag')) {
        $collection_flag = get_field('collection_flag');
    } else {
        $collection_flag = "";
    }

    $params = array('post-type' => $get_pt, 'product-name' => $get_slug, 'post-collection' => $collection_flag);
    //print_r($params);
    ?>
    <h1 class="section-title">Glass Types</h1>
        <?php 
    if(get_field('glass_description', 'option')) { ?>
    <p class="lead"><?php the_field('glass_description', 'option'); ?></p>
    <?php } ?>
    <?php
    $team_arg = array(
        'post_type' => 'glass_types',
        'collections' => $collection_flag,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_status' => 'publish',
        'posts_per_page' => 6,
    );
    $wp_team_query = new WP_Query($team_arg);
    //print_r($wp_team_query);
    $postx_counter = -1;
    ?>

    <ul class="product-slides">
        <?php
        while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
            $postx_counter++;
            ?>
            <li class="slide glass" data-count="<?php echo $postx_counter; ?>">
                <?php if (has_post_thumbnail()) : ?>
                    <a href="#modal" title="<?php the_title(); ?>" name="modal" data-image-url="<?php the_post_thumbnail_url('instagram-square'); ?>">
                        <?php the_post_thumbnail('instagram-square'); ?>
                    </a>
                <?php else : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                    </a>
                <?php endif; ?>
                <div class="prod-title"><?php the_title(); ?></div>
            </li>
        <?php endwhile; ?>
    </ul>
    <?php wp_reset_postdata(); ?>
    <!-- pass in the $params array and the URL -->
    <div class="more-prod-title">View all available glass types. <a href="<?php echo add_query_arg($params, '/glass-types'); ?>" class="btn-link" title="Glass Types"> Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
</section>
<?php wp_reset_postdata(); ?>