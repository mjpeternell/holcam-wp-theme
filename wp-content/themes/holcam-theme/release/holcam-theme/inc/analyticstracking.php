<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?php
//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'panoramaglobal.org') : ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91081270-1', 'auto');
  ga('send', 'pageview');

</script>
<?php else : ?>

<?php endif; ?>
