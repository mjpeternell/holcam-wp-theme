/*! Holcam WP Theme  - v0.1.0 - 2017-08-14
 * http://holcamDotcom
 * Copyright (c) 2017; * Licensed GPLv2+ */



/*! RoosterPark wpex_staticheader.js - v0.1.0 - 2016-04-01
 * http://roosterparkDotcom
 * Copyright (c) 2016; * Licensed GPLv2+ */

jQuery(function(){
    function wpex_staticheader() {
        var header_height = jQuery('.navbar').outerHeight();
        jQuery('#page').css({
            paddingTop: header_height

        });

        if (jQuery('#wpadminbar').length) {
            var admin_height = jQuery('#wpadminbar').outerHeight();
            jQuery('.navbar').css({
                position: 'fixed',
                top: admin_height
            });

        }
    }

    wpex_staticheader();

    jQuery(window).resize(function () {
        wpex_staticheader();
    });

    jQuery(window).bind('orientationchange', function (event) {
        var header_height = jQuery('.navbar').outerHeight();
        jQuery('#page').css({
            paddingTop: header_height
        });

        if (jQuery('#wpadminbar').length) {
            var admin_height = jQuery('#wpadminbar').outerHeight();
            jQuery('.navbar').css({
                position: 'fixed',
                top: admin_height
            });

        }
    });
    
});