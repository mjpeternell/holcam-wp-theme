<?php
/**
 * Template Name: Installation Documents Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <section class="col-fullbleed holcam-blog">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        get_template_part('template-parts/content', 'default-page');
                    endwhile;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="col-fullbleed install-docs">
        <div class="col-full">
            <div class="column-10 offset-1">
                <header class="single-header">
                    <h2 class="single-subtitle">Select a Document to Download</h2>
                </header>
                <ul class="download-links">
                    <?php
                    while (have_rows('download_links')): the_row();
                        $link_url = get_sub_field('link_url');
//                        echo '<pre>';
//                        print_r($link_url);
//                         echo '</pre>';
                        if ($link_url) {
                            $my_id2 = $link_url['ID'];
                            $url_file = $link_url['url'];
                            $title_file = $link_url['title'];
                            $caption_file = $link_url['caption'];
                            $desc = $link_url['description'];
                            $icon = $link_url['icon'];
                        }
                        ?>
                        <li class="link-item ">
                            <div class="column-1">
                                <?php if (wp_get_attachment_image($link_url['ID'])) { ?>
                                    <?php echo wp_get_attachment_image($link_url['ID'], 'thumbnail', "", array("class" => "img-thumbnail")); ?>
                                <?php } else { ?>
                                    <img src="<?php echo $icon; ?>" class="img-responsive"/>
                                <?php } ?>
                            </div> 
                            <div class="column-10">
                                <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>"><?php echo $title_file; ?> <i class="fa fa-download" aria-hidden="true"></i></a>
                                <div class="descrition"><?php echo $desc; ?></div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php
get_footer();


