<?php
/**
 * Template Name: Dealer Tools Home Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <?php
    global $current_user;
    wp_get_current_user();
    if (user_can($current_user, "dealer") || user_can($current_user, "administrator")) {
        ?>
        <section class="col-fullbleed holcam-blog">
            <div class="col-full">
                <div class="column-10 offset-1">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            get_template_part('template-parts/content', 'default-page');
                        endwhile;
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="col-fullbleed install-docs">
            <div class="col-full">
                <div class="column-10 offset-1">

                    <ul class="download-links">
                        <?php
                        $rlink_url = get_field('resources_link_url');
//                    echo '<pre>';
//                    print_r($rlink_url);
//                    echo '</pre>';
                        if ($rlink_url):
                            ?>
                            <header class="single-header">
                                <h2 class="single-subtitle">Visit our Dealer Tools Pages</h2>
                            </header>
                            <ul>
                                <?php foreach ($rlink_url as $rurl): ?>
                                    <?php
                                    $rurl = rtrim($rurl, "/");
                                    $s1clean = str_replace('-', ' ', $rurl);
                                    $str = preg_replace('/^.*\/\s*/', '', $s1clean);
                                    ?>
                                    <li>
                                        <a href="<?php echo $rurl ?>"><?php echo ucwords($str); ?> <i class="fa fa-link" aria-hidden="true"></i></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </section>
    <?php } else { ?>
        <section class="col-fullbleed holcam-blog">
            <div class="col-full">
                <div class="column-10 offset-1">
                    <header class="entry-header page-header">
                        <h1 class="entry-title">Dealer Tools Login</h1>
                        <p class="subtitle"><strong>You Need To Be Logged into the site inorder to gain access.</strong></p>
                    </header>
                    <div class="">
                        <p>Returning users please login below. First time users, skip to Dealer Request Form.</p>
                        <p><a class="btn btn-primary btn-sm" href="<?php echo wp_login_url(get_permalink()); ?>" role="button">Login <i class="fa fa-angle-double-right"></i></a>
                            <br/><br/><br/>
                        <h3>Dealer Request Form</h3>
                        <p>Please fill-out this quick request form for:</p>
                        <ul>
                            <li>Account setup to gain access to Dealers Tools Login</li>
                            <li>Information on becoming an Holcam dealer</li>
                            <li>Additional information that you have not found on this site</li>
                            <li>Recommending changes to this site or Holcam products</li>
                            <li>Giving general feedback</li>
                        </ul>
                        <br/>
                        <p><a href="/dealer-request-form/" class="btn btn-primary btn-sm">Dealer Request Form <i class="fa fa-angle-double-right"></i></a></p>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
</div>
<?php
get_footer();


