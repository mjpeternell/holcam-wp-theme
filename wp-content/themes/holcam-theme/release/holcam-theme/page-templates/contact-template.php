<?php
/**
 * Template Name: Contact Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax');
?>
<?php //get_template_part('inc/page-template-intro-content');  ?>
<?php
// Start the loop.
if (have_posts()) :
    ?>
<section class="col-fullbleed contact-page-intro">
    <div class="col-full">
        <div class="column-10 offset-1">
        <?php
        while (have_posts()) : the_post();
            // Include the page content template.
            //get_template_part('content', 'page');
            // End the loop.
            the_content();
        endwhile;
        ?>
        </div>
    </div>
</section>
<?php endif; ?>
    <?php get_template_part('inc/global','where-to-buy-cta'); ?>
    <?php get_template_part('inc/global','contact-cta'); ?>
<?php
get_footer();


