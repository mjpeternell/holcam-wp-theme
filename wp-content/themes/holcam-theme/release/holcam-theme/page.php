<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area  top">
    <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
    <div class="col-fullbleed holcam-blog">
        <div class = "column-10 offset-1 search-list">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('template-parts/content', 'default-page'); ?>
                <?php //comments_template('', true); ?>
            <?php endwhile; // end of the loop. ?>

        </div>
    </div>
</div>
<?php get_footer(); ?>