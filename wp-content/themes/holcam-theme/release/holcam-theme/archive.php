<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();

$page_color = "";
if (is_tax('configuration_tax')) {
    $page_color = "gun-metal";
} elseif (is_tax('enclosure_type_tax')) {
    $page_color = "gun-metal";
}
$pt_flag = ""
?>

<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="col-full">
                <div class="column-12 text-center">
                    <?php if (have_posts()) : ?>
                        <header class="column-12 text-center page-header <?php echo $page_color; ?>">
                            <div class="page-header-content">
                                <h1 class="page-title">
                                    <?php
                                    if (is_category()) {
                                        printf(__('Category Archives: %s', 'panorama'), '<span>' . single_cat_title('', false) . '</span>');
                                    } elseif (is_tag()) {
                                        printf(__('Tag Archives: %s', 'panorama'), '<span>' . single_tag_title('', false) . '</span>');
                                    } elseif (is_author()) {
                                        /* Queue the first post, that way we know
                                         * what author we're dealing with (if that is the case).
                                         */
                                        the_post();
                                        printf(__('Author Archives: %s', 'panorama'), '<span class="vcard"><a class="url fn n" href="' . get_author_posts_url(get_the_author_meta("ID")) . '" title="' . esc_attr(get_the_author()) . '" rel="me">' . get_the_author() . '</a></span>');
                                        /* Since we called the_post() above, we need to
                                         * rewind the loop back to the beginning that way
                                         * we can run the loop properly, in full.
                                         */
                                        rewind_posts();
                                    } elseif (is_day()) {
                                        printf(__('Daily Archives: %s', 'panorama'), '<span>' . get_the_date() . '</span>');
                                    } elseif (is_month()) {
                                        printf(__('Monthly Archives: %s', 'panorama'), '<span>' . get_the_date('F Y') . '</span>');
                                    } elseif (is_year()) {
                                        printf(__('Yearly Archives: %s', 'panorama'), '<span>' . get_the_date('Y') . '</span>');
                                    } elseif (post_type_exists('swing_doors')) {
                                        printf(__('%s', 'panorama'), '<span>' . post_type_archive_title() . '</span>');
                                        $pt_flag = "sd";
                                    } elseif (post_type_exists('sliding_doors')) {
                                        printf(__('%s', 'panorama'), '<span>' . post_type_archive_title() . '</span>');
                                    } else {
                                        _e('Archives', 'panorama');
                                    }
                                    ?>
                                    <?php
                                    if (is_tax()) {
                                        echo '<span>' . single_term_title() . '<br>Enclosures</span>';
                                    }
                                    ?>
                                </h1>
                                <?php
                                if (is_category()) {
                                    // show an optional category description
                                    $category_description = category_description();
                                    if (!empty($category_description))
                                        echo apply_filters('category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>');
                                } elseif (is_tag()) {
                                    // show an optional tag description
                                    $tag_description = tag_description();
                                    if (!empty($tag_description))
                                        echo apply_filters('tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>');
                                }
                                ?>
                                <?php
                                if (is_post_type_archive('swing_doors')) {
                                    $swing_doors_post = get_field('swing_doors_post', 'option');
                                    if ($swing_doors_post) {
                                        echo '<div class="post-description align-center">' . $swing_doors_post . '</div>';
                                    }
                                }
                                if (is_post_type_archive('sliding_doors')) {
                                    $sliding_door_post = get_field('sliding_door_post', 'option');
                                    if ($sliding_door_post) {
                                        echo '<div class="post-description align-center">' . $sliding_door_post . '</div>';
                                    }
                                }
                                if (is_post_type_archive('barn_doors')) {
                                    $barn_door_post = get_field('barn_door_post', 'option');
                                    if ($barn_door_post) {
                                        echo '<div class="post-description align-center">' . $barn_door_post . '</div>';
                                    }
                                }
                                ?>
                            </div>
                        </header><!-- .page-header -->
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-fullbleed holcam-blog">
                <div class="col-full">
                    <div class="column-10 offset-1">

                    </div>
                    <div class="column-12 filters">

                        <?php
                        if (is_post_type_archive('swing_doors')) {
                            echo '<h2 class="section-title align-center">Select a Swing Door Configuration and Collection</h2>';
                            echo '<h3 class="section-help">To see a complete list of Configurations. <a href="/our-configurations" target="_blank">Click Here</a></h3>';
                        }
                        if (is_post_type_archive('sliding_doors')) {
                            echo '<h2 class="section-title align-center">Select a Sliding Door Configuration and Collection</h2>';
                            echo '<h3 class="section-help">To see a complete list of Configurations. <a href="/our-configurations"  target="_blank">Click Here</a></h3>';
                        }
                        if (is_post_type_archive('barn_doors')) {
                            echo '<h2 class="section-title align-center">Select a Sliding Door Configuration and Collection</h2>';
                            echo '<h3 class="section-help">To see a complete list of Configurations. <a href="our-configurations"  target="_blank">Click Here</a></h3>';
                        }
                        ?>
                        <?php
                        if ((is_post_type_archive('swing_doors')) || (is_archive())) {
                            $tax_arg = array(
                                'post_type' => array('swing_doors')
                            );
                            $terms_cat = get_terms('configuration_tax');
                        }
                        if (is_post_type_archive('sliding_doors')) {
                            $tax_arg = array(
                                'post_type' => array('sliding_doors')
                            );
                            $terms_cat = get_terms('se_configuration_tax');
                        }
                        if (is_post_type_archive('barn_doors')) {
                            $tax_arg = array(
                                'post_type' => array('barn_doors')
                            );
                            $terms_cat = get_terms('bd_configuration');
                        }
                        $wp_ctax_query = new WP_Query($tax_arg);
                        $ctax_counter = 0;

                        if (!empty($terms_cat) && !is_wp_error($terms_cat)) {
                            echo '<div class="button-group myfilter" data-filter-group="config">';
                            echo '<div class="column-12"><h4>Configuration: <a id="reset-btn-config" class="conf-tag-all selected" href="#filter" title="All" data-filter=""><span>Show All</span></a></h4></div>';

                            echo '<ul id="filters01" class="pg-tag-cloud option-set">';
                            foreach ($terms_cat as $term) {
                                $ctax_counter++;
                                echo '<li>';
                                echo '<a id="cs-tag' . $ctax_counter . '" href="#" class="conf-tag" title="' . strtoupper(str_replace('-', ' ', $term->slug)) . '" data-filter=".' . str_replace(' ', '-', $term->name) . '">';
                                echo '<div class="img-icon ' . strtoupper(str_replace('-', ' ', $term->slug)) . '"> </div>';
                                echo '<span>' . $term->name . '</span>';
                                echo '</a>';
                                echo '</li>';
                            }
                            echo '</ul>';
                            echo '</div>';
                        }
                        ?>
                        <?php
                        $taxonomy = 'collections';
                        $queried_term = get_query_var($taxonomy);

                        $slug_name = "";
                        if (is_post_type_archive('barn_doors')) {
                            $slug_name = array('eclipse');
                        }
                        if (is_post_type_archive('swing_doors')) {
                            $slug_name = array('brilliance', 'classic', 'distinctive', 'distinctive-elite', 'eurolite');
                        }
                        if (is_post_type_archive('sliding_doors')) {
                            $slug_name = array('brilliance', 'classic', 'distinctive', 'distinctive-elite', 'eurolite');
                        }

                        $tax_term_arg = array(
                            'taxonomy' => array('collections'),
                            'slug' => $slug_name
                        );

                        $terms_col = get_terms($tax_term_arg);
                         rsort($terms_col);
                        if (!empty($terms_col) && !is_wp_error($terms_col)) {

                            echo '<div class="button-group" data-filter-group="enclosure">';
                            echo '<div class="column-12"><h4>Collections: <a id="reset-btn-col" class="encl-tag-all selected" href="#filter" title="All" data-filter=""><span>Show All</span></a></h4></div>';
                            echo '<ul id="filters02" class="pg-tag-cloud option-set">';
                             $coltax_counter = 0;
                            foreach ($terms_col as $col_term) {
                                $coltax_counter++;
                                echo '<li><a id="cs-tag' . $coltax_counter . '" href="#" class="encl-tag" title="' . strtoupper(str_replace('-', ' ', $col_term->slug)) . '" data-filter=".' . str_replace(' ', '-', $col_term->slug) . '"><div class="tname"><span>' . $col_term->name . '</span></div><div class="desc"><span>' . $col_term->description . '</span></div></a></li>';
                            }
                            echo '</ul>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                    <div class="column-10 offset-1 iso-grid-title"><h2 class="iso-title">Filtered Products</h2></div>
                    <div class="column-10 offset-1">
                        <div class="cs-box cs-grid-wrapper">
                            <div id="gridPortfolio" class="grid portfolio" >
                                <div class="gutter-sizer"></div>
                                <?php if (have_posts()) : ?>


                                    <?php //rooster_park_content_nav('nav-above');           ?>

                                    <?php /* Start the Loop */ ?>
                                    <?php while (have_posts()) : the_post(); ?>

                                        <?php
                                        /* Include the Post-Format-specific template for the content.
                                         * If you want to overload this in a child theme then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
                                        get_template_part('content', get_post_format());
                                        ?>

                                    <?php endwhile; ?>

                                    <?php panorama_content_nav('nav-below'); ?>

                                <?php else : ?>

                                    <?php get_template_part('template-parts/no-results', 'archive'); ?>

                                <?php endif; ?>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->  
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php get_footer(); ?>