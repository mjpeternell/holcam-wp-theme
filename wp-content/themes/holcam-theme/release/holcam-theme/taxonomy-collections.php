<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();

$pt_flag = ""
?>

<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="col-full">
                <div class="column-12 text-center">
                    <?php if (have_posts()) : ?>
                        <header class="page-header">
                            <div class="page-header-content">
                                <h1 class="page-title">
                                    <?php
                                    if (is_tax()) {
                                        echo '<span>' . single_term_title() . ' Collection</span>';
                                    }
                                    ?>      
                                </h1>
                                <?php
                                if (is_category()) {
                                    // show an optional category description
                                    $category_description = category_description();
                                    if (!empty($category_description))
                                        echo apply_filters('category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>');
                                } elseif (is_tag()) {
                                    // show an optional tag description
                                    $tag_description = tag_description();
                                    if (!empty($tag_description))
                                        echo apply_filters('tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>');
                                }
                                ?>
                                <?php
                                if (is_tax('collections')) {
                                    $col_name = get_queried_object()->slug;
                                    $barn_door_post = get_field($col_name, 'option');
                                    if ($barn_door_post) {
                                        echo '<div class="post-description align-center">' . $barn_door_post . '</div>';
                                    }
                                }
                                ?>
                            </div>
                        </header><!-- .page-header -->
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-fullbleed holcam-blog">
                <div class="col-full">
                    <div class="column-10 offset-1">

                    </div>
                    <div class="column-10 offset-1 filters">

                        <?php
                        if (is_post_type_archive('swing_doors')) {
                            echo '<h2 class="section-title align-center">Select a Swing Door Configuration and Collection</h2>';
                        }
                        if (is_post_type_archive('sliding_doors')) {
                            echo '<h2 class="section-title align-center">Select a Sliding Door Configuration and Collection</h2>';
                        }
                        if (is_post_type_archive('barn_doors')) {
                            echo '<h2 class="section-title align-center">Select a Sliding Door Configuration and Collection</h2>';
                        }
                        ?>
                        <?php
                        if ((is_post_type_archive('swing_doors')) || (is_archive())) {
                            $tax_arg = array(
                                'post_type' => array('swing_doors')
                            );
                            $terms_cat = get_terms('configuration_tax');
                        }
                        if (is_post_type_archive('sliding_doors')) {
                            $tax_arg = array(
                                'post_type' => array('sliding_doors')
                            );
                            $terms_cat = get_terms('se_configuration_tax');
                        }
                        if (is_post_type_archive('barn_doors')) {
                            $tax_arg = array(
                                'post_type' => array('barn_doors')
                            );
                            $terms_cat = get_terms('bd_configuration');
                        }
                        $wp_ctax_query = new WP_Query($tax_arg);
                        $ctax_counter = 0;

//                        if (!empty($terms_cat) && !is_wp_error($terms_cat)) {
//                            echo '<div class="button-group myfilter" data-filter-group="config">';
//                            echo '<div class="column-12"><h4>Configuration: <a id="reset-btn" class="conf-tag-all selected" href="#filter" title="All" data-filter=""><span>Show All</span></a></h4></div>';
//
//                            echo '<ul id="filters01" class="pg-tag-cloud option-set">';
//                            foreach ($terms_cat as $term) {
//                                $ctax_counter++;
//                                echo '<li>';
//                                echo '<a id="cs-tag' . $ctax_counter . '" href="#" class="conf-tag" title="' . strtoupper(str_replace('-', ' ', $term->slug)) . '" data-filter=".' . str_replace(' ', '-', $term->name) . '">';
//                                echo '<div class="img-icon ' . strtoupper(str_replace('-', ' ', $term->slug)) . '"> </div>';
//                                echo '<span>' . $term->name . '</span>';
//                                echo '</a>';
//                                echo '</li>';
//                            }
//                            echo '</ul>';
//                            echo '</div>';
//                        }
                        ?>
                        <?php
//                        $wp_cctax_query = new WP_Query($tax_arg);
//                        $cctax_counter = 0;
//                        $terms_cccat = get_terms('collections');
//                        asort($terms_cccat);
//
//                        if (!empty($terms_cccat) && !is_wp_error($terms_cccat)) {
//
//                            echo '<div class="button-group" data-filter-group="enclosure">';
//                            echo '<div class="column-12"><h4>Collections: <a id="reset-btn" class="encl-tag-all selected" href="#filter" title="All" data-filter=""><span>Show All</span></a></h4></div>';
//                            echo '<ul id="filters02" class="pg-tag-cloud option-set">';
//                            foreach ($terms_cccat as $ccterm) {
//                                $cctax_counter++;
//                                echo '<li><a id="cs-tag' . $cctax_counter . '" href="#" class="encl-tag" title="' . strtoupper(str_replace('-', ' ', $ccterm->slug)) . '" data-filter=".' . str_replace(' ', '-', $ccterm->slug) . '"><div class="tname"><span>' . $ccterm->name . '</span></div><div class="desc"><span>' . $ccterm->description . '</span></div></a></li>';
//                            }
//                            echo '</ul>';
//                            echo '</div>';
//                        }
                        $taxonomy = 'collections';
                        $queried_term = get_query_var($taxonomy);

                        $slug_name = "";
                        if (is_post_type_archive('barn_doors')) {
                            $slug_name = array('eclipse');
                        }
                        if (is_post_type_archive('swing_doors')) {
                            $slug_name = array('brilliance', 'classic', 'distinctive', 'distinctive-elite', 'eurolite-12', 'eurolite-38');
                        }
                        if (is_post_type_archive('sliding_doors')) {
                            $slug_name = array('brilliance', 'classic', 'distinctive', 'distinctive-elite', 'eurolite-12', 'eurolite-38');
                        }

                        $tax_term_arg = array(
                            'taxonomy' => array('collections'),
                            'slug' => $slug_name
                        );

                        $terms_col = get_terms($tax_term_arg);

//                        if (!empty($terms_col) && !is_wp_error($terms_col)) {
//
//                            echo '<div class="button-group" data-filter-group="enclosure">';
//                            echo '<div class="column-12"><h4>Collections: <a id="reset-btn" class="encl-tag-all selected" href="#filter" title="All" data-filter=""><span>Show All</span></a></h4></div>';
//                            echo '<ul id="filters02" class="pg-tag-cloud option-set">';
//                             $coltax_counter = 0;
//                            foreach ($terms_col as $col_term) {
//                                $coltax_counter++;
//                                echo '<li><a id="cs-tag' . $coltax_counter . '" href="#" class="encl-tag" title="' . strtoupper(str_replace('-', ' ', $col_term->slug)) . '" data-filter=".' . str_replace(' ', '-', $col_term->slug) . '"><div class="tname"><span>' . $col_term->name . '</span></div><div class="desc"><span>' . $col_term->description . '</span></div></a></li>';
//                            }
//                            echo '</ul>';
//                            echo '</div>';
//                        }
                        ?>
                    </div>
                    <div class="column-10 offset-1 iso-grid-title"><h2 class="iso-title">Configurations</h2></div>
                    <div class="column-10 offset-1">
                        <div class="cs-box cs-grid-wrapper">
                            <div id="gridPortfolio" class="grid portfolio" >
                                <div class="gutter-sizer"></div>
                                <?php if (have_posts()) : ?>


                                    <?php //rooster_park_content_nav('nav-above');           ?>

                                    <?php /* Start the Loop */ ?>
                                    <?php while (have_posts()) : the_post(); ?>

<?php
if (get_post_type(get_the_ID()) == 'swing_doors') {
    $my_config_cat = get_the_terms(get_the_ID(), 'configuration_tax');
}

if (get_post_type(get_the_ID()) == 'sliding_doors') {
    $my_config_cat = get_the_terms(get_the_ID(), 'se_configuration_tax');
}

if (get_post_type(get_the_ID()) == 'barn_doors') {
    $my_config_cat = get_the_terms(get_the_ID(), 'bd_configuration');
}

//if ($my_config_cat && !is_wp_error($my_config_cat)) {
//    $config_links = array();
//    foreach ($my_config_cat as $term) {
//        $config_links[] = $term->name;
//    }
//    $my_config = join(" ", $config_links);
//}
?>
<?php
$my_encl_cat = get_the_terms(get_the_ID(), 'enclosure_type_tax');
if ($my_encl_cat && !is_wp_error($my_encl_cat)) :
    $encl_links = array();
    foreach ($my_encl_cat as $termx) {
        $encl_links[] = $termx->slug;
    }
    $my_encl = join(" ", $encl_links);
    ?>
<?php endif; ?>
<?php
$my_coll_cat = get_the_terms(get_the_ID(), 'collections');
if ($my_coll_cat && !is_wp_error($my_coll_cat)) :
    $coll_links = array();
    foreach ($my_coll_cat as $termcc) {
        $coll_links[] = $termcc->slug;
    }
    $my_coll = join(" ", $coll_links);
    ?>
<?php endif; ?>
<?php if ($my_coll == $col_name) { ?>
    

<article id="post-<?php the_ID(); ?>" class="grid-item isotope-item  <?php printf(esc_html__('%s', 'textdomain'), esc_html($my_coll)); ?>">
    <div class="tile-inner">
        <header class="entry-header">

            <?php if (in_array(get_post_type(), array('post', 'barn_doors', 'swing_doors', 'sliding_doors', 'configuration_tax', 'enclosure_type_tax'))) : ?>
                <div class="entry-meta">
                    <div class="blog-image">
                        <?php
                        echo '<div class="enclosure-links">';
                        if (is_post_type_archive('swing_doors') || is_post_type_archive('sliding_doors') || is_post_type_archive('barn_doors') || is_archive()) {
                            echo get_the_term_list($post->ID, 'enclosure_type_tax', '', ', ');
                        }
                        echo '</div>';
                        ?>
                        <?php if (has_post_thumbnail()) : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <?php the_post_thumbnail('iso-grid-thumb'); ?>
                            </a>
                        <?php else : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                            </a>
                        <?php endif; ?>
                    </div>
                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" class="name truncate-120" title="<?php echo esc_attr(sprintf(__('%s', 'panorama'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                    <?php //panorama_posted_on();    ?>

                </div> 

            <?php endif; ?>
        </header><!-- .entry-header -->
        <?php if (is_search()) : // Only display Excerpts for Search      ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div><!-- .entry-summary -->
        <?php else : ?>
<!--            <div class="entry-content"> </div>-->
        <?php endif; ?>

        <footer class="entry-meta">
            <?php if (in_array(get_post_type(), array('post', 'swing_doors', 'sliding_doors')) || is_archive()) : // Hide category and tag text for pages on Search    ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list(__(', ', 'panorama'));
                if ($categories_list && panorama_categorized_blog()) :
                    ?>
                    <span class="cat-links">
                        <?php printf(__('Posted in %1$s', 'panorama'), $categories_list, '<span class="sep"> | </span>'); ?>
                    </span>
                <?php endif; // End if categories      ?>

                <?php
                echo '<span class="tags-links">';
                if (is_post_type_archive('swing_doors') || is_archive()) {
                    echo get_the_term_list($post->ID, 'configuration_tax', '', ', ');
                }
                if (is_post_type_archive('sliding_doors') || is_archive()) {
                    echo get_the_term_list($post->ID, 'se_configuration_tax', '', ', ');
                }
                echo '</span>'
                ?>

            <?php endif; // End if 'post' == get_post_type()   ?>
            <?php //edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>', 'panorama'), '<span class="edit-link">', '</span>'); ?>
        </footer><!-- .entry-meta -->
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
<?php } ?>

                                    <?php endwhile; ?>

                                    <?php panorama_content_nav('nav-below'); ?>

                                <?php else : ?>

                                    <?php get_template_part('template-parts/no-results', 'archive'); ?>

                                <?php endif; ?>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->  
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php get_footer(); ?>