
<?php
/**
 * The template for displaying the header.
 *
 * @package Panorama
 * @since 0.1.0
 */
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $themeUrl = get_stylesheet_directory_uri(); ?>

        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/android-chrome-384x384.png" sizes="384x384">

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $themeUrl; ?>/assets/favicons/apple-touch-icon-180x180.png">
        <link rel="mask-icon" href="<?php echo $themeUrl; ?>/assets/favicons/safari-pinned-tab.svg"  color="#5bbad5">

        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo $themeUrl; ?>/assets/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" sizes="16x16" href="<?php echo $themeUrl; ?>/assets/favicons/favicon.ico">

        <link rel="manifest" href="<?php echo $themeUrl; ?>/assets/favicons/manifest.json">
        <link rel="mask-icon" href="<?php echo $themeUrl; ?>/assets/favicons/mstile-150x150.png" color="#5bbad5">

        <meta name="theme-color" content="#ffffff">
        <?php wp_head(); ?>
        <!--[if lt IE 9]>
    <script src="<?php echo get_stylesheet_directory_uri() . "/assets/js/vendor/html5shiv.js"; ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . "/assets/js/vendor/respond.min.js"; ?>"></script>
<![endif]--> 
    </head>
    <body <?php body_class(); ?>>
        <?php include_once("inc/analyticstracking.php") ?>
        <div class="navigation-wrapper utility"><div class="phone-num">
                <ul class="phone-links">
                    <li>Ph: <a href="tel:800-843-3332" >800-843-3332</a></li>
                    <li><a href="/where-to-buy">Where To Buy</a></li>
                </ul>
            </div>
            <div class="social">
                <?php if (have_rows('social_links', 'option')): ?>
                    <ul class="social-links">
                        <?php
                        while (have_rows('social_links', 'option')): the_row();
                            // vars
                            $social_title = get_sub_field('social_title', 'option');
                            $social_Icon = get_sub_field('social_icon', 'option');
                            $social_icon_url = get_sub_field('social_icon_url', 'option');
                            ?>
                            <li>
                                <?php if ($social_icon_url): ?>
                                    <a href="<?php echo $social_icon_url; ?>" title="<?php echo $social_title; ?>"  target="_blank">
                                    <?php endif; ?>
                                    <i class="fa <?php echo $social_Icon ?>" aria-hidden="true"></i>
                                    <?php if ($social_icon_url): ?>
                                    </a>
                                <?php endif; ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="search"><?php get_template_part('template-parts/search', 'form'); ?></div>
        </div>
        <header class="navigation banner headroom" role="banner" id="Header">

            <div class="navigation-wrapper">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="logo"  rel="home">
                    <?php get_template_part('inc/holcam-logo')//header_image(); ?>
                </a>
                <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="hamburger-bar"></span>
                    <span class="hamburger-bar"></span>
                    <span class="hamburger-bar"></span>
                </a>
                <?php
                $args = array(
                    'theme_location' => 'primary',
                    'container' => 'nav',
                    'container_id' => 'navbar',
                    'container_class' => 'navbar-collapse collapse',
                    'menu_class' => 'navigation-menu',
                    'fallback_cb' => '',
                    'menu_id' => 'js-navigation-menu',
                    'walker' => new rooster_park_navwalker()
                );
                wp_nav_menu($args);
                ?>
            </div>
        </header>
        <div id="page" class="hfeed site">
            <main id="main" class="site-main white" role="main">
