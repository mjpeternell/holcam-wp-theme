<?php
$section_title_con = get_field('section_title_con');
?>
<?php if ($section_title_con): ?>
    <h2 class="section-header white align-center"><?php echo $section_title_con; ?></h2>
<?php endif; ?>
<?php if (have_rows('cta_repeater')): ?>
    <div class="row">
        <?php
        while (have_rows('cta_repeater')): the_row();
            // vars
            $image = get_sub_field('cta_icon');
            $title = get_sub_field('cta_title');
            $excerpt = get_sub_field('cta_excerpt');
            $link = get_sub_field('cta_link');
            $link_label = get_sub_field('cta_link_label');
            ?>

            <div class="column-4 align-center">
                <?php if ($image): ?>
                    <div class="featured-icon">
                        <a href="<?php echo $link; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                    </div>
                <?php endif; ?>
                <?php if ($title): ?>
                    <h3 class="content-box-heading"><?php echo $title; ?></h3>
                <?php endif; ?>
                <?php if ($excerpt): ?>
                    <div class="content-container">
                        <?php echo $excerpt; ?>
                    </div>
                <?php endif; ?>
                <?php if ($link): ?>
                    <a class="cs-link" href="<?php echo $link; ?>"><?php echo $link_label; ?></a>
                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>

