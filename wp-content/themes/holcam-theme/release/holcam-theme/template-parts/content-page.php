<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>

<?php
/**
 * The template used for displaying page content in page-template-default
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>

<!-- content-page.php -->
<div class="column-7">

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="single-header">
            <h1 class="single-title"><?php the_title(); ?></h1>
            <?php
            $post_subtitle = get_field("post_subtitle");
            if ($post_subtitle) {
                ?>
                <h2 class="single-subtitle"><?php echo $post_subtitle; ?></h2>
            <?php } else { ?>
                <h2 class="single-subtitle">&nbsp;</h2>
            <?php } ?>
        </header>
        <div class="entry-content white">
            <?php the_content(); ?>
            <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link'); ?>
        </div>
    </article><!-- #post-<?php the_ID(); ?> -->
</div>

<?php if (has_post_thumbnail()) { ?>
    <div class="column-5 sidebar">
        <?php the_post_thumbnail('hero-cta-1024'); ?>
    </div>
<?php } else { ?>
    <div class="column-4 offset-1 sidebar">
        <?php get_sidebar(); ?>
    </div>
<?php } ?>