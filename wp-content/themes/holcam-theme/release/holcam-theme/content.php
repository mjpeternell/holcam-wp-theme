<?php
/**
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<?php
if (get_post_type(get_the_ID()) == 'swing_doors') {
    $my_config_cat = get_the_terms(get_the_ID(), 'configuration_tax');
}

if (get_post_type(get_the_ID()) == 'sliding_doors') {
    $my_config_cat = get_the_terms(get_the_ID(), 'se_configuration_tax');
}

if (get_post_type(get_the_ID()) == 'barn_doors') {
    $my_config_cat = get_the_terms(get_the_ID(), 'bd_configuration');
}

if ($my_config_cat && !is_wp_error($my_config_cat)) :
    $config_links = array();
    foreach ($my_config_cat as $term) {
        $config_links[] = $term->name;
    }
    $my_config = join(" ", $config_links);
    ?>
<?php endif; ?>
<?php
$my_encl_cat = get_the_terms(get_the_ID(), 'enclosure_type_tax');
if ($my_encl_cat && !is_wp_error($my_encl_cat)) :
    $encl_links = array();
    foreach ($my_encl_cat as $termx) {
        $encl_links[] = $termx->slug;
    }
    $my_encl = join(" ", $encl_links);
    ?>
<?php endif; ?>
<?php
$my_coll_cat = get_the_terms(get_the_ID(), 'collections');
if ($my_coll_cat && !is_wp_error($my_coll_cat)) :
    $coll_links = array();
    foreach ($my_coll_cat as $termcc) {
        $coll_links[] = $termcc->slug;
    }
    $my_coll = join(" ", $coll_links);
    ?>
<?php endif; ?>

<article id="post-<?php the_ID(); ?>" class="grid-item isotope-item  <?php printf(esc_html__('%s', 'textdomain'), esc_html($my_config)); ?> <?php printf(esc_html__('%s', 'textdomain'), esc_html($my_coll)); ?>">
    <div class="tile-inner">
        <header class="entry-header">

            <?php if (in_array(get_post_type(), array('post', 'barn_doors', 'swing_doors', 'sliding_doors', 'configuration_tax', 'enclosure_type_tax'))) : ?>
                <div class="entry-meta">
                    <div class="blog-image">
                        <?php
                        echo '<div class="enclosure-links">';
                        if (is_post_type_archive('swing_doors') || is_post_type_archive('sliding_doors') || is_post_type_archive('barn_doors') || is_archive()) {
                            echo get_the_term_list($post->ID, 'enclosure_type_tax', '', ', ');
                        }
                        echo '</div>';
                        ?>
                        <?php if (has_post_thumbnail()) : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <?php the_post_thumbnail('iso-grid-thumb'); ?>
                            </a>
                        <?php else : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                            </a>
                        <?php endif; ?>
                    </div>
                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" class="name truncate-120" title="<?php echo esc_attr(sprintf(__('%s', 'panorama'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                    <?php //panorama_posted_on();    ?>

                </div> 

            <?php endif; ?>
        </header><!-- .entry-header -->
        <?php if (is_search()) : // Only display Excerpts for Search      ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div><!-- .entry-summary -->
        <?php else : ?>
<!--            <div class="entry-content"> </div>-->
        <?php endif; ?>

        <footer class="entry-meta">
            <?php if (in_array(get_post_type(), array('post', 'swing_doors', 'sliding_doors')) || is_archive()) : // Hide category and tag text for pages on Search    ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list(__(', ', 'panorama'));
                if ($categories_list && panorama_categorized_blog()) :
                    ?>
                    <span class="cat-links">
                        <?php printf(__('Posted in %1$s', 'panorama'), $categories_list, '<span class="sep"> | </span>'); ?>
                    </span>
                <?php endif; // End if categories      ?>

                <?php
                echo '<span class="tags-links">';
                if (is_post_type_archive('swing_doors') || is_archive()) {
                    echo get_the_term_list($post->ID, 'configuration_tax', '', ', ');
                }
                if (is_post_type_archive('sliding_doors') || is_archive()) {
                    echo get_the_term_list($post->ID, 'se_configuration_tax', '', ', ');
                }
                echo '</span>'
                ?>

            <?php endif; // End if 'post' == get_post_type()   ?>
            <?php //edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>', 'panorama'), '<span class="edit-link">', '</span>'); ?>
        </footer><!-- .entry-meta -->
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
