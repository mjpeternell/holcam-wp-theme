<?php

/**
 * Holcam functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Holcam
 * @since Holcam 1.0
 */
// Useful global constants
define('HOLCAM_VERSION', '0.1.0');

/**
 * Add humans.txt to the <head> element.
 */
function holcam_header_meta() {
    $humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';
    echo apply_filters('holcam_humans', $humans);
}

add_action('wp_head', 'holcam_header_meta');

/**
 * Set the theme's image size.
 *
 * @since Holcam 1.0
 */
add_image_size('hero-cta-480', 480, 292, array('left', 'top'));
add_image_size('hero-cta-680', 680, 400, array('left', 'top'));
add_image_size('hero-cta-1024', 1024, 864, array('left', 'top'));
add_image_size('hero-cta-1280', 1280, 1080, array('left', 'top'));
add_image_size('iso-grid-thumb', 175, 175, array('center', 'top'));
add_image_size('instagram-square', 480, 480, array('left', 'top'));
add_image_size('instagram-square-a', 680, 680, array('left', 'top'));
add_image_size('hero-img-medium-b', 768);
add_image_size('hero-img-large-a', 992);
add_image_size('hero-img-large-b', 1200);
add_image_size('hero-imag-xlarge', 1354);


add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3);

function remove_thumbnail_dimensions($html, $post_id, $post_image_id) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Holcam 1.0
 */
if (!isset($content_width))
    $content_width = 640; /* pixels */

if (!function_exists('holcam_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * @since Holcam 1.0
     */
    function holcam_setup() {
        /**
         * Custom template tags for this theme.
         */
        require 'inc/navwalker.php';

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'panorama'),
            'footer_1' => __('Footer Links Left ', 'panorama'), // secondary nav in footer
            'footer_2' => __('Footer Links Middle', 'panorama'), // secondary nav in footer
            'footer_3' => __('Footer Links Right', 'panorama') // secondary nav in footer
        ));

        /**
         * Custom template tags for this theme.
         */
        require( get_template_directory() . '/inc/template-tags.php' );

        /**
         * Extra featured Images.
         */
        require( get_template_directory() . '/inc/extra-featured-images.php' );


        /**
         * Custom functions that act independently of the theme templates
         */
        require( get_template_directory() . '/inc/extras.php' );

        /**
         * Customizer additions
         */
        require( get_template_directory() . '/inc/customizer.php' );


        /**
         * Add Thumbnails in Manage Posts/Pages List
         */
        require( get_template_directory() . '/inc/admin-feature-image.php' );

        /**
         * ACF Options Page Setup
         */
        require( get_template_directory() . '/inc/admin-acf-options-page.php' );

        /**
         * Add SVG Support to media library
         */
        require( get_template_directory() . '/inc/admin-svg-suport-media.php' );

        /**
         * WordPress.com-specific functions and definitions
         */
        //require( get_template_directory() . '/inc/wpcom.php' );

        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain('panorama', get_template_directory() . '/languages');

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support('post-thumbnails');

        function custom_admin_head() {
            $css = '';
            $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
            echo '<style type="text/css">' . $css . '</style>';
        }

        /**
         * Add support for the Aside Post Formats
         */
        add_theme_support('post-formats', array('aside',));
    }

endif; // holcam_setup
add_action('after_setup_theme', 'holcam_setup');

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since Holcam 1.0
 */
function panorama_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'panorama'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));

    register_sidebar(array(
        'id' => 'footer-widget-1',
        'name' => __('Footer Widget 1', 'sparkling'),
        'description' => __('Used for footer widget area', 'sparkling'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'id' => 'footer-widget-2',
        'name' => __('Footer Widget 2', 'sparkling'),
        'description' => __('Used for footer widget area', 'sparkling'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'id' => 'footer-widget-3',
        'name' => __('Footer Widget 3', 'sparkling'),
        'description' => __('Used for footer widget area', 'sparkling'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'id' => 'footer-widget-4',
        'name' => __('Footer Widget 4', 'sparkling'),
        'description' => __('Used for footer widget area', 'sparkling'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
}

add_action('widgets_init', 'panorama_widgets_init');

/**
 * Enqueue scripts and styles
 */
function panorama_scripts() {
    /**
     * Check for Dev Environment if true load unminified scripts and css else load minified versions.
     */
    $minified = "";
    //echo $myDomain = $_SERVER['HTTP_HOST'];

    if (($_SERVER['HTTP_HOST'] === 'holcam.mattpeternell.net') || $_SERVER['HTTP_HOST'] === 'holcam.com' || $_SERVER['HTTP_HOST'] === 'beta.holcam.com') {
        $minified = ".min";
    } else {
        $minified = "";
    }

    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome' . $minified . '.css');
    wp_enqueue_style('panorama_sass', get_template_directory_uri() . '/assets/css/theme-style' . $minified . '.css');
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.1.min.js');
    wp_enqueue_script('modernizer', get_template_directory_uri() . '/assets/js/vendor/modernizr.min.js');
    wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/assets/js/theme-scripts' . $minified . '.js');

    wp_enqueue_script('headhesive-js', get_template_directory_uri() . '/assets/js/vendor/headroom' . $minified . '.js');
    wp_enqueue_script('jq-headhesive-js', get_template_directory_uri() . '/assets/js/vendor/jQuery.headroom.js');
    wp_enqueue_script('succinct-truncate', get_template_directory_uri() . "/assets/js/vendor/jQuery.succinct.min.js");
    wp_enqueue_script('theme_extras', get_template_directory_uri() . '/assets/js/theme-plugins' . $minified . '.js');
    wp_enqueue_script('count-up', get_template_directory_uri() . '/assets/js/vendor/count.js');
    wp_enqueue_script('inview-js', get_template_directory_uri() . '/assets/js/vendor/jquery.inview.min.js');
    wp_enqueue_script('isotope-js', get_template_directory_uri() . '/assets/js/vendor/isotope.pkgd.min.js', array(), HOLCAM_VERSION, true);
    wp_enqueue_script('packery-js', get_template_directory_uri() . '/assets/js/vendor/packery-mode.pkgd.js', array(), HOLCAM_VERSION, true);
    wp_enqueue_script('lazy-js', get_template_directory_uri() . '/assets/js/vendor/jquery.lazyload.min.js', array(), HOLCAM_VERSION, true);
    wp_enqueue_script('gallery-js', get_template_directory_uri() . '/assets/js/vendor/lightslider' . $minified . '.js', array(), HOLCAM_VERSION, true);
    wp_enqueue_script('bxslider-js', get_template_directory_uri() . '/assets/js/vendor/jquery.bxslider' . $minified . '.js', array(), HOLCAM_VERSION, true);
    wp_enqueue_script('easing-js', get_template_directory_uri() . '/assets/js/vendor/jquery.easing.1.3' . $minified . '.js', array(), HOLCAM_VERSION, true);
    wp_enqueue_script('isotope-jss', get_template_directory_uri() . '/assets/js/isotope-masonary-work' . $minified . '.js', array(), HOLCAM_VERSION, true);

//    if (is_singular() && comments_open() && get_option('thread_comments')) {
//        wp_enqueue_script('comment-reply');
//    }
//
//    if (is_singular() && wp_attachment_is_image()) {
//        wp_enqueue_script('keyboard-image-navigation', get_template_directory_uri() . '/assets/js/vendor/keyboard-image-navigation.js', array('jquery'), '20120202');
//    }
}

add_action('wp_enqueue_scripts', 'panorama_scripts');

/**
 * Set CSS Color class in <body> tag using ACF
 */
require( get_template_directory() . '/inc/admin-acf-body-class.php' );

/**
 * Breadcrumb function
 */
require( get_template_directory() . '/inc/breadcrumb.php' );



add_action('init', 'gp_register_taxonomy_for_object_type');

function gp_register_taxonomy_for_object_type() {
    register_taxonomy_for_object_type('post_tag', 'swing_doors', 'sliding_doors', 'configuration_tax', 'enclosure_type_tax', 'glass_types');
}

function namespace_add_custom_types($query) {
    if (is_category() || is_tag() && empty($query->query_vars['suppress_filters'])) {
        $query->set('post_type', array(
            'post', 'nav_menu_item', 'swing_doors', 'sliding_doors', 'configuration_tax', 'enclosure_type_tax', 'glass_types'
        ));
        return $query;
    }
}

add_filter('pre_get_posts', 'namespace_add_custom_types');

function add_excerpt_class($excerpt) {
    $excerpt = str_replace("<p", "<p class=\"excerpt\"", $excerpt);
    return $excerpt;
}

add_filter("the_excerpt", "add_excerpt_class");

/**
 * This function modifies the main WordPress query to include an array of 
 * post types instead of the default 'post' post type.
 *
 * @param object $query  The original query.
 * @return object $query The amended query.
 */
function searchfilter($query) {

    if ($query->is_main_query() && !is_admin() && $query->is_search()) {
        $query->set('post_type', array('swing_doors', 'sliding_doors', 'configuration_tax', 'enclosure_type_tax', 'glass_types'));
        // Change the query parameters
        $query->set('posts_per_page', 10);
    }
    return $query;
}

add_filter('pre_get_posts', 'searchfilter');


/* ------------------------------------*\
  Actions + Filters + ShortCodes
  \*------------------------------------ */

// Add Actions
//add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
//add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
//add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
//add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
//add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
//add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
//add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
//add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

function disable_wp_emojicons() {

    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    add_filter('emoji_svg_url', '__return_false');

    // filter to remove TinyMCE emojis
    //add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

add_action('init', 'disable_wp_emojicons');


// Add Filters
//add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
//add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
//add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
//add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
//add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
//add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
//add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
////add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
//add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
//add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
//add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
//add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
//add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether
// Shortcodes
//add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
//add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.
// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]


function custom_query_vars_filter($vars) {
    $vars[] = 'product-name';
    $vars[] .= 'post-type';
    $vars[] .= 'post-collection';
    return $vars;
}

add_filter('query_vars', 'custom_query_vars_filter');
