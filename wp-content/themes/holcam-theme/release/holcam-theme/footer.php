<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>

<footer id="colophon" class="site-footer container-fluid" role="contentinfo">
    <div class="site-info">
        <div class="row">
            <?php if (is_active_sidebar('footer-widget-1')) : ?>
                <div class="column-3 footer-widget" role="complementary">
                    <?php dynamic_sidebar('footer-widget-1'); ?>
                </div><!-- .widget-area .first -->
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-widget-2')) : ?>
                <div class="column-3 footer-widget" role="complementary">
                    <?php dynamic_sidebar('footer-widget-2'); ?>
                </div><!-- .widget-area .second -->
            <?php endif; ?>
            <div class="column-3 footer-widget" role="complementary">
                
                <?php if (is_active_sidebar('footer-widget-3')) : ?>
                    <?php dynamic_sidebar('footer-widget-3'); ?>
                <?php endif; ?>
                <?php if (have_rows('social_links', 'option')): ?>
                    <div id="SocialLinks" class="widget widget_text">
                        <h3 class="widgettitle">Holcam Social Links</h3>
                        <ul class="social-links-list">
                            <?php
                            while (have_rows('social_links', 'option')): the_row();
                                // vars
                                $social_icon = get_sub_field('social_icon', 'option');
                                $social_title = get_sub_field('social_title', 'option');
                                $social_icon_url = get_sub_field('social_icon_url', 'option');
                                ?>
                                <li class="social-link-item">  <a href="<?php echo $social_icon_url; ?>" title="<?php echo $social_title; ?>" target="_blank">
                                        <i class="fa <?php echo $social_icon; ?>" aria-hidden="true"></i>    
                                    </a>
                                    <?php if ($social_icon_url): ?>

                                    <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
                <!-- .widget-area .third -->
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-widget-4')) : ?>
                <div class="column-3 footer-widget" role="complementary">
                    <?php dynamic_sidebar('footer-widget-4'); ?>
                </div><!-- .widget-area .third -->
            <?php endif; ?>
        </div>
        <?php //do_action('panorama_credits', 'option'); ?>
    </div>
    <div class="copyright">
        <div class="copyright-inner">
            <?php esc_attr_e('Copyright ©', 'preference'); ?><?php _e(date('Y')); ?> <?php bloginfo('name'); ?>  All Rights Reserved
        </div>
    </div>
</footer>

</main><!-- #main .site-main -->
</div><!-- #page .hfeed .site -->

<div class="scroll-to-top">
    <i class="fa fa-angle-up"></i>
</div>
<?php wp_footer(); ?>
</body>
</html>