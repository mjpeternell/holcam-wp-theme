module.exports = function (grunt) {

    // Project configuration
    grunt.config('phplint', {
        options: {
            phpCmd: "/usr/bin/php",
            phpArgs: {
                '-ldf': true,
                '-d': ["display_errors", "display_startup_errors"]
            }
        },
        all: {
            src: '<%= paths.php.files %>'
        }
    });

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: '\n\n\n\n',
                stripBanners: false,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n\n\n\n'
            },
            roosterpark: {
                src: ['assets/js/src/theme-scripts.js'],
                dest: 'assets/js/theme-scripts.js'
            },
            extras: {
                src: ['assets/js/src/plugin-staticheader.js'],
                dest: 'assets/js/panorama-plugins.js'
            },
            extras1: {
                src: ['assets/js/src/plugin-parallax.js'],
                dest: 'assets/js/theme-plugins.js'
            },
            extras2: {
                src: ['assets/js/src/isotope-masonary-work.js'],
                dest: 'assets/js/isotope-masonary-work.js'
            }
        },
//        shell: {
//            options: {
//                stderr: false
//            },
//            target: {
//                command: 'cp -a ~/Sites/panorama/wp-content/themes/panorama/release/panorama-park-theme'
//            }
//        },
        jshint: {
            all: [
                'Gruntfile.js',
                'assets/js/src/**/*.js',
                'assets/js/test/**/*.js'
            ],
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                globals: {
                    exports: true,
                    module: false,
                    $: false,
                    jQuery: false,
                    console: false,
                    document: false,
                    window: false,
                    google: false,
                    alert: false,
                    setInterval: false,
                    setTimeout: false
                }
            }
        },
        uglify: {
            options: {
                mangle: false,
                separator: '\n\n',
                report: 'min',
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            my_target_1: {
                files: {
                    'assets/js/theme-scripts.min.js': ['assets/js/theme-scripts.js']
                }
            },
            my_target_2: {
                files: {
                    'assets/js/theme-plugins.min.js': ['assets/js/theme-plugins.js']
                }
            },
            my_target_3: {
                files: {
                    'assets/js/isotope-masonary-work.min.js':  ['assets/js/isotope-masonary-work.js']
                }
            }
        },
        test: {
            files: ['assets/js/test/**/*.js']
        },
        sass: {
            all: {
                files: {
                    'assets/css/theme-style.css': 'sass/theme-style.scss'
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            target: {
                expand: true,
                cwd: 'assets/css/',
                src: ['*.css', '!*.min.css'],
                dest: 'assets/css/',
                ext: '.min.css'
            }
        },
        watch: {
            sass: {
                files: ['sass/**/*.scss'],
                tasks: ['sass', 'cssmin'],
                options: {
                    debounceDelay: 500
                }
            },
            scripts: {
                files: ['assets/js/src/**/*.js', 'assets/js/vendor/**/*.js'],
                tasks: ['jshint', 'concat', 'uglify'],
                options: {
                    debounceDelay: 500
                }
            },
            phplint: {
                files: ['**/*.php'], // which files to watch,
                tasks: ['phplint'],
                options: {
                    spawn: false
                }
            }
        },
        clean: {
            main: ['release/<%= pkg.name %>']
        },
        copy: {
            // Copy the plugin to a versioned release directory
            main: {
                src: [
                    '**',
                    '!bower_components/**',
                    '!node_modules/**',
                    '!nbproject/**',
                    '!release/**',
                    '!.git/**',
                    '!css/src/**',
                    '!js/src/**',
                    '!img/src/**',
                    '!Gruntfile.js',
                    '!package.json',
                    '!.gitignore',
                    '!.gitmodules'
                ],
                dest: 'release/<%= pkg.name %>/'
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'zip',
                    archive: './release/<%= pkg.name %>.<%= pkg.version %>.zip'
                },
                expand: true,
                cwd: 'release/<%= pkg.name %>/',
                src: ['**/*'],
                dest: '<%= pkg.name %>/'
            }
        },
        phplint: {
            options: {
                stdout: true,
                stderr: true,
                swapPath: '/tmp'
            },
            files: ['*.php', '**/*.php', '!node_modules/**/*.php'] // which files to watch
        }
    });

    // Load other tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-phplint');
    grunt.loadNpmTasks('grunt-contrib-compress');

    // Default task.
    grunt.registerTask('default', ['jshint', 'clean', 'concat', 'uglify', 'sass', 'cssmin', 'compress']);
    grunt.registerTask('build', ['default', 'copy']);
    grunt.registerTask('css', ['sass', 'cssmin', 'phplint']);
    grunt.registerTask('php', ['phplint']);

    grunt.util.linefeed = '\n';
};