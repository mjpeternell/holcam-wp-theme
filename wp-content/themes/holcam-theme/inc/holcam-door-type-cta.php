<?php
/*
 * Case Study Loop: includes ACF, title, copy and link.
 * Pages: Homepage,
 */
?>
<section id="c" class="col-fullbleed white three-col-section collections">
    <div class="column-10  offset-1">
        <?php get_template_part('template-parts/acf', 'repeater'); ?>
    </div>
</section>