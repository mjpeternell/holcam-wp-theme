<?php
/*
 * Page Intro Content: includes WP page content.
 * Pages: Homepage,
 * 
 */
?>

<?php
$my_id = "introSection";
?>
<section id="<?php echo $my_id; ?>" class="intro-section col-fullbleed">
    <div class="col-full">
        <div class="column-10 offset-1">
            <?php
// Start the Loop.
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'page');
            endwhile;
            ?>
        </div>
    </div>
</section>


