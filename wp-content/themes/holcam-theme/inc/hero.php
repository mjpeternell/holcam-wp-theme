<?php
/*
 * Hero Image: includes ACF Hero image, title, subtitle and copy.
 * Pages: Homepage,
 */
?>

<section id="js-home-hero" class="col-fullbleed hero-slider">
    <div class="col-full">
            <?php if (have_rows('page_hero_repeater')): ?>

                <ul class="bxslider">

                    <?php
                    while (have_rows('page_hero_repeater')): the_row();

                        // vars
                        $hero_title = get_sub_field('hero_title');
                        $hero_subtitle = get_sub_field('hero_subtitle');
                        $hero_image = get_sub_field('hero_image');
                        $hero_link = get_sub_field('hero_link');
                        ?>

                        <li class="slide">
                            <div class="caption">
                                <h1><?php echo $hero_title; ?></h1>
                                <h2><?php echo $hero_subtitle; ?></h2>
                                <?php if($hero_link) { ?>
                                <a href="<?php echo $hero_link; ?>" title="Learn More" class="btn btn-primary" role="button">Learn More</a>
                                <?php } ?>
                            </div>
                            <img src="<?php echo $hero_image['url']; ?>" alt="<?php echo $hero_image['alt'] ?>" />
                        </li>

                    <?php endwhile; ?>

                </ul>

            <?php endif; ?>
          
        </div>
</section>