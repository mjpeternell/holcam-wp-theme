<?php
/**
 * The template used for displaying home apge intro content in homepage template
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<section class="intro-section col-fullbleed">
    <div class="col-full">
        <div class="column-10 offset-1">
            <!-- content-page.php -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                if (has_post_thumbnail()) {
                    the_post_thumbnail('hero-cta-1024');
                }
                ?>
                <div class="entry-content white">
                    <?php the_content(); ?>
                    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
                    <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link'); ?>
                </div>
            </article><!-- #post-<?php the_ID(); ?> -->
        </div>
    </div>
</section>

