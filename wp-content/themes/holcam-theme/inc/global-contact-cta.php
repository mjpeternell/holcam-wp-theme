<section id="Cool-Stuff" class="col-fullbleed contact-cta">
    <div class="col-full">
        <?php
        $contact_cta_section_title = get_field('contact_cta_section_title','option');
        $contact_cta_link = get_field('contact_cta_link','option');
        $contact_cta_link_label = get_field('contact_cta_link_label', 'option')
        ?>
        <?php
        if ($contact_cta_section_title) {
            echo '<h2 class="section-header white align-center">' . $contact_cta_section_title . '</h2>';
        }
        ?>
        <div class="column-12 text-center"> 
            <?php
            if ($contact_cta_link) {
                echo '<a href="'.$contact_cta_link.'" class="btn btn-primary" role="button">' . $contact_cta_link_label . '</a>';
            }
            ?>
        </div>
    </div>
</section>