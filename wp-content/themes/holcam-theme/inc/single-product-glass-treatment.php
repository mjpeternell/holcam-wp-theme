<section id="glassTreatment" class="column-10 offset-1 single-team-post">
    <h1 class="section-title">Glass Treatments</h1>

    <?php
    $team_arg = array(
        'post_type' => 'glass_treatments',
        'orderby' => 'title',
        'order' => 'ASC',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );
    $wp_team_query = new WP_Query($team_arg);
    //print_r($wp_team_query);
    $postx_counter = -1;
    ?>

    <ul class="product-slides">
        <?php
        while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
            $postx_counter++;
            ?>
            <li class="slide treatments" data-count="<?php echo $postx_counter; ?>">

                <div class="prod-title"></div>
                <div class="column-8">
                    <h3><?php the_title(); ?></h3>
                    <div class="prod-title"><?php the_content(); ?></div>
                </div>
                <div class="column-4 vert-align">
                    <?php if (has_post_thumbnail()) : ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php the_post_thumbnail('instagram-square'); ?>
                        </a>
                    <?php else : ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                        </a>
                    <?php endif; ?>
                </div>
            </li>
        <?php endwhile; ?>
    </ul>
    <?php wp_reset_postdata(); ?>
</section>