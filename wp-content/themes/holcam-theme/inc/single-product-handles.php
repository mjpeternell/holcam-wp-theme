<section id="handles" class="column-10 offset-1 single-team-post">
    <?php
    // URL Parameters to create return link on archive page
    $get_pt = get_post_type();
    $get_slug = get_post_field('post_name', get_post());
    $params = array('post-type' => $get_pt, 'product-name' => $get_slug);

//ACF Sets Product Options filtering flag
    if (get_field('collection_flag')) {
        $collection_flag = get_field('collection_flag');
    } else {
        $collection_flag = "";
    }
    ?>
    <h1 class="section-title">Handles</h1>
            <?php 
    if(get_field('handles_description', 'option')) { ?>
    <p class="lead"><?php the_field('handles_description', 'option'); ?></p>
    <?php } ?>
    <?php
    $team_arg = array(
        'post_type' => 'handles',
        //'metal_finish_type' => 'anodized',
        'orderby' => 'title',
        'order' => 'ASC',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );
    $wp_team_query = new WP_Query($team_arg);
    //print_r($wp_team_query);
    $postx_counter = -1;
    ?>

    <ul class="product-slides">
        <?php
        while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
            $postx_counter++;
            ?>
            <li class="slide handles" data-count="<?php echo $postx_counter; ?>">
                <?php if (has_post_thumbnail()) : ?>
                    <a href="#modal" title="<?php the_title(); ?>" name="modal" data-image-url="<?php the_post_thumbnail_url('instagram-square'); ?>">
                        <?php the_post_thumbnail('instagram-square'); ?>
                    </a>
                <?php else : ?>
        <!--                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">-->
                    <img src="/wp-content/themes/holcam-theme/assets/images/placeholder-175x.png" class="img-responsive"  alt="PlaceHolder Image"/>
                    <!--                                        </a>-->
                <?php endif; ?>
                <div class="prod-title"><?php the_title(); ?></div>
            </li>
        <?php endwhile; ?>
    </ul>
    <?php wp_reset_postdata(); ?>
    <div class="more-prod-title">View all available handle options. <a href="<?php echo add_query_arg($params, '/handles'); ?>" class="btn-link" title="Handles"> Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
</section>