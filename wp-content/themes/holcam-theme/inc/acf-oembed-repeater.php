<?php
if (have_rows('instuctional_videos')):
    echo '<div class="embed-section">';
    ?>
    <!--    <header class="single-header">
            <h2 class="single-subtitle">Select a Document to Download</h2>
        </header>-->
    <?php
// loop through the rows of data
    while (have_rows('instuctional_videos')) : the_row();
        $pf_video_title = get_sub_field('pf_video_title');
        ?>
        <div class="video-title"><?php echo $pf_video_title; ?></div>
        <?php
        echo '<div class="embed-container">';

        // get iframe HTML
        $iframe = get_sub_field('pf_video');
        //print_r($iframe);
        // use preg_match to find iframe src
        preg_match('/src="(.+?)"/', $iframe, $matches);
        $src = $matches[1];
        // add extra params to iframe src
        $params = array(
            'controls' => 1,
            'hd' => 1,
            'autohide' => 1
        );
        $new_src = add_query_arg($params, $src);
        $iframe = str_replace($src, $new_src, $iframe);

        // add extra attributes to iframe html
        $attributes = 'frameborder="0"';
        $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
        echo $iframe;
        
        echo '</div>';
    endwhile;
    echo '</div>';
else :
// no rows found
endif;
?>