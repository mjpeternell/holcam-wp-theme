<?php $themeUrl = get_stylesheet_directory_uri(); ?>
<?php
$small_logo = get_field("small_logo", 'options');
$large_logo = get_field("large_logo", 'options');
if ($small_logo) { 
    ?>
    <img id="panoLogoSmall" class="holcam-logo sm-logo" src="<?php echo $small_logo; ?>"/>
    <img id="panoLogoLarge" class="holcam-logo md-logo" src="<?php echo $large_logo; ?>"/>
<?php } else { ?>
    <span>Add a Small and Logo</span>
<?php } ?>
