<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<?php
// URL Parameters to create return link on archive page
$get_pt = get_post_type();
$get_slug = get_post_field('post_name', get_post());
$params = array('post-type' => $get_pt, 'product-name' => $get_slug);

//ACF Sets Product Options filtering flag

if (get_field('collection_flag')) {
    $collection_flag = get_field('collection_flag');
} else {
    $collection_flag = "";
}
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <nav class="column-10 offset-1 breadcrumb-nav"><?php holcam_breadcrumbs(); ?></nav>
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <section class="column-10 offset-1 single-team-post">
                        <div class="row">
                            <div class="column-6">
                                <?php //the_post_thumbnail(); ?>
                                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                    <li data-thumb="<?php the_post_thumbnail_url('iso-grid-thumb'); ?>" >
                                        <img src="<?php the_post_thumbnail_url('instagram-square-a'); ?>"/>
                                        <?php if (!have_rows('popular_finishes_repeater')): ?>
                                        </li>   
                                    <?php endif; ?>
                                    <?php if (have_rows('popular_finishes_repeater')): ?>
                                        <div class="caption-container">
                                            <div class="caption-content">
                                                <div class="caption-title">Popular Finishes <span>(Click an image to change finish) </span></div>
                                            </div>
                                        </div>
                                        </li>   
                                        <?php
                                        while (have_rows('popular_finishes_repeater')): the_row();
                                            $popular_finish_image = get_sub_field('popular_finish_image');
                                            $popular_finish_label = get_sub_field('popular_finish_label');
                                            $url = $popular_finish_image['url'];
                                            $alt = $popular_finish_image['alt'];
                                            // thumbnail
                                            $thumb_size = 'iso-grid-thumb';
                                            $thumb_img = $popular_finish_image['sizes'][$thumb_size];
                                            // full Img
                                            $full_size = 'instagram-square-a';
                                            $full_img = $popular_finish_image['sizes'][$full_size];
                                            ?>

                                            <li data-thumb="<?php echo $thumb_img; ?>" >
                                                <?php if ($popular_finish_image): ?>
                                                    <img src="<?php echo $full_img; ?>" alt="<?php echo $alt ?>" class="img-responsive"/>
                                                    <div class="caption-container">
                                                        <div class="caption-content">
                                                            <div class="caption-title"><?php echo $popular_finish_label; ?></div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                            <div class="column-6 entry-content">
                                <header class="single-header">
                                    <h1 class="single-title"><?php the_title(); ?></h1>
                                    <?php
                                    $post_subtitle = get_field("post_subtitle");
                                    if ($post_subtitle) {
                                        ?>
                                        <h2 class="single-subtitle"><?php echo $post_subtitle; ?></h2>
                                    <?php } else { ?>
                                        <h2 class="single-subtitle">&nbsp;</h2>
                                    <?php } ?>
                                    <?php
                                    $current_part_number = get_field("current_part_number");
                                    $obsolete_part_number = get_field("obsolete_part_number");
                                    if ($current_part_number && $obsolete_part_number) {
                                        ?>
                                         <div class="part-number">Current Model: <?php echo $current_part_number; ?> <?php echo ' | Previous Model: ' . $obsolete_part_number; ?></div>
                                    <?php } ?>
                                </header>
                                <div >
                                    <?php the_content(); ?>
                                </div><!-- .entry-content -->
                            </div>
                        </div>
                    </section>
                    <nav id="prodHotLinks" class="single-product-links column-10 offset-1 single-team-post">
                        <ul class="sp-link-menu">
                            <li class="sp-link-item"><a href="#metal" title="">Metal</a></li>
                            <li class="sp-link-item"><a href="#glassTypes" title="">Glass Types</a></li>
                            <li class="sp-link-item"><a href="#glassTreatment" title="">Glass Treatment</a></li>
                            <li class="sp-link-item"><a href="#handles" title="">Handles</a></li>
                            <li class="sp-link-item"><a href="#documentation" title="">Documentation</a></li>
                        </ul>
                    </nav>
                    <?php get_template_part('inc/single-product', 'metal'); ?>
                    <?php get_template_part('inc/single-product', 'glass'); ?>
                    <?php get_template_part('inc/single-product', 'glass-hd'); ?>
                    <?php get_template_part('inc/single-product', 'glass-treatment'); ?>
                    <?php get_template_part('inc/single-product', 'handles'); ?>
                    <?php get_template_part('inc/single-product', 'documentation'); ?>
                    <footer class="column-10 offset-1 align-center">
                        <?php edit_post_link(__('Edit', 'panorama'), '<span class="edit-link">', '</span>'); ?>
                    </footer>
                </article><!-- #post-<?php the_ID(); ?> -->
            <?php endwhile; // end of the loop.          ?>


        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/global', 'where-to-buy-cta'); ?>
<?php get_template_part('inc/global', 'contact-cta'); ?>
<?php get_footer(); ?>