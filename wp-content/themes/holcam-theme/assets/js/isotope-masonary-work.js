/*! Holcam WP Theme  - v0.1.0 - 2017-08-14
 * http://holcamDotcom
 * Copyright (c) 2017; * Licensed GPLv2+ */



/*! panorama Park Theme  - v0.1.0 - 2017-02-02
 * http://roosterparkDotcom
 * Copyright (c) 2017; * Licensed GPLv2+ */


function initIsotope() {
    // init Isotope
    var $imgs = jQuery('img.lazy');
    var $grid = jQuery('.grid');

    var filters = {};
    var $window = jQuery(window);

    $grid.isotope({
        itemSelector: '.grid-item',
        percentPosition: false,
        masonry: {
            columnWidth: '.grid-item',
            gutter: '.gutter-sizer'
        },
        sortBy : 'name',
        sortAscending: false,
        getSortData: {
            name: '.name' // text from querySelector
        }
        
    });

    $imgs.lazyload({
        load: function () {
            $grid.isotope('layout');
        }
    });

    // bind filter button click
    jQuery('.filters').on('click', 'a', function () {
       
        var $this = $(this);
        // get group key
        var $buttonGroup = $this.parents('.button-group');
        var filterGroup = $buttonGroup.attr('data-filter-group');
        // set filter for group
        filters[ filterGroup ] = $this.attr('data-filter');
        //console.log(filters);
        // combine filters
        var filterValue = concatValues(filters);
        $grid.isotope({filter: filterValue});
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });

// flatten object by concatting values
    function concatValues(obj) {
        var value = '';
        for (var prop in obj) {
            value += obj[ prop ];
        }
        return value;
    }
    // change is-checked class on buttons
    jQuery('.option-set').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        });
    });

//    // change is-checked class on buttons
    jQuery('.cat-item-set .cat-item').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        });
    });

    jQuery('#mobileFilterBtn').on('click', function (e) {
        e.preventDefault();
        jQuery(".grid-controls").toggleClass('showFilter').animate({"right": "0px"}, 300);
        jQuery(".cat-child a").click(function () {
            jQuery("#filterMenu").hide("fast").removeClass('showFilter');
        });
    });

    jQuery('#reset-btn-config, #reset-btn-col').click(function () {
        jQuery('#filters02, #filters01').find('.selected').removeClass('selected');
        //$buttonGroup.find('.selected').removeClass('selected');
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });
}
jQuery(window).on('load', function () {
    initIsotope();
    var yPo = jQuery('.page-header').height();
    jQuery('.filters').on('click', 'a', function () {
        jQuery('html, body, #gridPortfolio').animate({scrollTop: yPo}, 800);
    });
});
 